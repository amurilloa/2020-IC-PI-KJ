/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iexamen.ejerciciouno;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

/**
 *
 * @author amurilloa
 */
public class Empleado {

    private String cedula;
    private String nombre;
    private LocalDate fechaIngreso;
    private String correo;
    private String telefono;
    private char genero;

    public Empleado() {
    }

    public Empleado(String cedula, String nombre, LocalDate fechaIngreso, String correo, String telefono, char genero) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.fechaIngreso = fechaIngreso;
        this.correo = correo;
        this.telefono = telefono;
        this.genero = genero;
    }

    public int getAnnos() {
        return Period.between(fechaIngreso, LocalDate.now()).getYears();
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return cedula + " - " + nombre + " (" + correo + ", " + telefono + ")";
    }

}
