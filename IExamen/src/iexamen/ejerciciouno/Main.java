/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iexamen.ejerciciouno;

import java.time.LocalDate;

/**
 *
 * @author amurilloa
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();
        String[] opciones = {"1. Registrar", "2. Buscar", "3. Listar", "4.Salir"};
        String[] generos = {"1. Femenino", "2. Masculino"};
        APP:
        while (true) {
            int op = Util.menuOpciones(opciones, "Seleccione una opción:");

            switch (op) {
                case 0:
                    String cedula = Util.leer("Cédula:");
                    String nombre = Util.leer("Nombre:");
                    LocalDate fechaIngreso = Util.leerFecha("Fecha Ingreso(23/01/2020):");
                    String correo = Util.leer("Correo:");
                    String telefono = Util.leer("Teléfono:");
                    int sex = Util.menuOpciones(generos, "Género:");
                    char genero = sex == 0 ? 'F' : 'M';
                    Empleado e = new Empleado(cedula, nombre, fechaIngreso, correo, telefono, genero);
                    if(log.registrar(e)){
                        Util.mostrar("Empleado registrado con éxito!!");
                    } else {
                        Util.mostrar("Favor intente nuevamente");
                    
                    }
                    break;
                case 1:
                    Util.mostrar(log.buscar());
                    break;
                case 2:
                    Util.mostrar(log.listar());
                    break;
                case 3:
                    break APP;

            }
        }
    }
}
