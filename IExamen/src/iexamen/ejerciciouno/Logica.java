/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iexamen.ejerciciouno;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author amurilloa
 */
public class Logica {

    private Empleado[] empleados;

    public Logica() {
        empleados = new Empleado[15];
        datosPrueba();
    }

    private void datosPrueba() {
        registrar(new Empleado("201470062", "Roberto Murillo Alfaro", LocalDate.parse("2017-02-23"), "allanmual@gmail.com", "8526-2638", 'M'));
        registrar(new Empleado("202470962", "Allan Murillo Alfaro", LocalDate.now(), "allanmual@gmail.com", "8526-2638", 'M'));
        registrar(new Empleado("203470862", "Juan Murillo Alfaro", LocalDate.parse("2016-02-23"), "allanmual@gmail.com", "8526-2638", 'M'));
        registrar(new Empleado("204470762", "Pedro Murillo Alfaro", LocalDate.now(), "allanmual@gmail.com", "8526-2638", 'M'));
        registrar(new Empleado("205470662", "Luisa Murillo Alfaro", LocalDate.now(), "allanmual@gmail.com", "8526-2638", 'F'));
        registrar(new Empleado("206470562", "Maria Murillo Alfaro", LocalDate.parse("2013-06-23"), "allanmual@gmail.com", "8526-2638", 'F'));
        registrar(new Empleado("207470462", "Jose Murillo Alfaro", LocalDate.now(), "allanmual@gmail.com", "8526-2638", 'M'));
        registrar(new Empleado("208470362", "Lauren Murillo Alfaro", LocalDate.now(), "allanmual@gmail.com", "8526-2638", 'F'));
        registrar(new Empleado("209470262", "Maritza Murillo Alfaro", LocalDate.now(), "allanmual@gmail.com", "8526-2638", 'F'));
        registrar(new Empleado("200470162", "Luis Murillo Alfaro", LocalDate.now(), "allanmual@gmail.com", "8526-2638", 'M'));
    }

    /**
     * Registra un empleado
     *
     * @param emp datos del empleado a registrar
     * @return true si logra registrar el empleado, false caso contrario
     */
    public boolean registrar(Empleado emp) {
        for (int i = 0; i < empleados.length; i++) {
            if (empleados[i] == null) {
                empleados[i] = emp;
                return true;
            }
        }
        return false;
    }

    public String buscar() {
        String txt = "";
        for (Empleado empleado : empleados) {
            if (empleado != null) {
                int canAnnos = empleado.getAnnos();
                if (canAnnos >= 3 && canAnnos <= 5) {
                    txt += empleado + "\n";
                }
            }
        }
        return txt;
    }

    public String listar() {
        String txt = "";
        for (Empleado empleado : empleados) {
            if (empleado != null) {
                txt += empleado + "\n";
            }
        }
        return txt;
    }

}
