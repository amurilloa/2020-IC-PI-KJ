/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iexamen.ejerciciodos;

/**
 *
 * @author amurilloa
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();
        String[] opciones = {"1. Generar Arreglo", "2. Sumatoria Primos", "3. Lotto", "4.Salir"};
        int[] numeros = {1, 3, 5, 6, 7, 7};

        APP:
        while (true) {
            int op = Util.menuOpciones(opciones, "Seleccione una opción:");

            switch (op) {
                case 0:
                    numeros = log.generar(10, 30);
                    Util.mostrar(log.imprimir(numeros));
                    break;
                case 1:
                    Util.mostrar(log.sumatoria(numeros));
                    break;
                case 2:
                    int[] nums = new int[5];
                    for (int i = 0; i < nums.length; i++) {
                        nums[i] = Util.leerInt("#" + (i + 1));
                    }
                    Util.mostrar("Usted ha ganado la suma de: " + log.lotto(nums));
                    break;
                case 3:
                    break APP;

            }
        }
    }
}
