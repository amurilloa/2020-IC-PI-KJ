/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iexamen.ejerciciodos;

import iexamen.ejerciciouno.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.swing.JOptionPane;

/**
 *
 * @author amurilloa
 */
public class Util {

    private static final String TITULO = "Examen UTN - v0.1";

    public static String leer(String mensaje) {
        return JOptionPane.showInputDialog(null, mensaje, TITULO, JOptionPane.QUESTION_MESSAGE);
    }

    public static LocalDate leerFecha(String mensaje) {
        String fecha = JOptionPane.showInputDialog(null, mensaje, TITULO, JOptionPane.QUESTION_MESSAGE);
        return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public static int leerInt(String mensaje) {
        return Integer.parseInt(JOptionPane.showInputDialog(null, mensaje, TITULO, JOptionPane.QUESTION_MESSAGE));
    }

    public static double leerDouble(String mensaje) {
        return Double.parseDouble(JOptionPane.showInputDialog(null, mensaje, TITULO, JOptionPane.QUESTION_MESSAGE));
    }

    public static void mostrar(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, TITULO, JOptionPane.INFORMATION_MESSAGE);
    }

    public static void error(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, TITULO, JOptionPane.ERROR_MESSAGE);
    }

    public static boolean confirmar(String pregunta) {
        int res = JOptionPane.showConfirmDialog(null, pregunta, "Banco UTN - v0.1", JOptionPane.YES_NO_OPTION);
        return res == JOptionPane.YES_OPTION;
    }

    public static int menuOpciones(String[] opciones, String mensaje) {
        String res = (String) JOptionPane.showInputDialog(null, mensaje,
                TITULO, JOptionPane.QUESTION_MESSAGE,
                null, opciones, opciones[0]);
        for (int i = 0; i < opciones.length; i++) {
            if (opciones[i].equals(res)) {
                return i;
            }
        }
        return -1;
    }
}
