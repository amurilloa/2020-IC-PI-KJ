/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iexamen.ejerciciodos;

/**
 *
 * @author amurilloa
 */
public class Logica {

    public Logica() {
    }

    public int[] generar(int tam, int max) {
        int[] arr = new int[tam];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * max) + 1;
        }
        return arr;
    }

    public boolean esPrimo(int numero) {
        int con = 0;
        for (int i = 1; i <= numero; i++) {
            if (numero % i == 0) {
                con++;
            }
        }
        return con == 2;
    }

    public String sumatoria(int[] arr) {
        String txt = "";
        int total = 0;

        for (int num : arr) {
            if (esPrimo(num)) {
                total += num;
                txt += num + "+";
            }
        }

        txt = txt.substring(0, txt.length() - 1) + "=" + total;

        return txt;
    }

    public int lotto(int[] numeros) {
        int[] rifados = {12,32,34,45,56};//generar(5, 99);
        System.out.println(imprimir(rifados));
        System.out.println(imprimir(numeros));

        int cant = 0;
        for (int i = 0; i < numeros.length; i++) {
            for (int l = 0; l < rifados.length; l++) {
                if (numeros[i] == rifados[l]) {
                    cant++;
                }
            }
        }

        if (cant == 5) {
            return 500000;
        } else if (cant == 4) {
            return 200000;
        } else if (cant == 3) {
            return 100000;
        } else if (cant == 2) {
            return 50000;
        }

        return 0;
    }

    public String imprimir(int[] arr) {
        String txt = "|";
        for (int i : arr) {
            txt += " " + i + " |";
        }
        return txt;
    }

}
