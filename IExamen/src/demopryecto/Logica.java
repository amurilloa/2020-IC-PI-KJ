/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demopryecto;

/**
 *
 * @author amurilloa
 */
public class Logica {

    private Persona[] personas;
    private Mascota[] mascotas;

    private final String RUTA_P = "datos/personas.txt";
    private final String RUTA_M = "datos/mascotas.txt";

    public Logica() {
        personas = new Persona[100];
        mascotas = new Mascota[20];
        cargarP();
    }

    public void registrarP(Persona per) {
        for (int i = 0; i < personas.length; i++) {
            if (personas[i] == null) {
                personas[i] = per;
                break;
            }
        }
        guardarP();
    }

    public void registrarM(Mascota mas) {
        for (int i = 0; i < mascotas.length; i++) {
            if (mascotas[i] == null) {
                mascotas[i] = mas;
                break;
            }
        }
        guardarM();
    }

    public void eliminar(String ced) {
        for (int i = 0; i < personas.length; i++) {
            if (personas[i] != null && personas[i].getCedula().equals(ced)) {
                personas[i] = null;
                break;
            }
        }
        guardarP();
    }

    private void cargarP() {
        ManejoArchivos ma = new ManejoArchivos();
        String txt = ma.leer(RUTA_P);
        String[] lineas = txt.split("\n");
        for (String per : lineas) {
            String[] datos = per.split(",");
            registrarP(new Persona(datos[0], datos[1]));
        }
    }

    private void guardarP() {
        String txt = "";
        for (Persona per : personas) {
            if (per != null) {
                txt += per.getData();
            }
        }
        ManejoArchivos ma = new ManejoArchivos();
        ma.escribir(RUTA_P, txt);
    }

    private void guardarM() {
        String txt = "";
        for (Mascota mas : mascotas) {
            if (mas != null) {
                txt += mas.getData();
            }
        }
        ManejoArchivos ma = new ManejoArchivos();
        ma.escribir(RUTA_M, txt);
    }

    public String imprimirP() {
        String txt = "";
        for (Persona p : personas) {
            if (p != null) {
                txt += p + "\n";
            }
        }
        return txt;
    }

    public String imprimirM() {
        String txt = "";
        for (Mascota m : mascotas) {
            if (m != null) {
                txt += m + "\n";
            }
        }
        return txt;
    }

}
