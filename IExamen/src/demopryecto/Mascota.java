/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demopryecto;

/**
 *
 * @author amurilloa
 */
public class Mascota {

    private String duenno;
    private String nombre;
    private String raza;
    private String color;

    public Mascota() {
    }

    public Mascota(String duenno, String nombre, String raza, String color) {
        this.duenno = duenno;
        this.nombre = nombre;
        this.raza = raza;
        this.color = color;
    }

    public String getData() {
        return String.format("%s,%s,%s,%s\n", duenno, nombre, raza, color);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Mascota{" + "nombre=" + nombre + ", raza=" + raza + ", color=" + color + '}';
    }

}
