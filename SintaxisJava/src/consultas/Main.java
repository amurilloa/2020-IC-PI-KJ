/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultas;

import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static boolean esPar(int numero) {
        return numero % 2 == 0;
    }

    public static void main(String[] args) {
        int n1 = 7;

        boolean x = n1 % 2 == 0;

        if (x) {
            System.out.println("Es par");
        } else {
            System.out.println("Es impar");
        }

        boolean salir = false;

        while (!salir) {
            System.out.println("En el While");
            int res = JOptionPane.showConfirmDialog(null, "Desea Salir?");
            salir = res == JOptionPane.YES_OPTION;
        }

        int inicio = 100;
        int cantidad = 50;
        
        for (int i = inicio; i < inicio+cantidad; i++) {
            if (esPar(i)) {
                System.out.println(i + " es par");
            }
            
        }

    }

}
