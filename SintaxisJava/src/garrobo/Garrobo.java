/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garrobo;

/**
 *
 * @author ALLAN
 */
public class Garrobo {

    //Atributos
    private String nombre;
    private int tiempo;
    private int distancia;

    //Constructores 
    
    
    //Métodos 
    public int calcularDistancia(int tiempo) {
        double dist = calcVelocidad() * tiempo;
        return (int) Math.round(dist);
    }

    public int calcularTiempo(int distancia) {
        double tiempo = distancia / calcVelocidad();
        return (int) Math.round(tiempo);
    }

    private double calcVelocidad() {
        return (double) distancia / tiempo;
    }

    //Getters y Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }
    
    //toString

}
