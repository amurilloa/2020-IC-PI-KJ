/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garrobo;

import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class PruebaGarrobo {

    public static void main(String[] args) {
        Garrobo g1 = new Garrobo();
        Garrobo g2 = new Garrobo();

        String nom = JOptionPane.showInputDialog("Nombre del Garrobo #1");
        int dis = Integer.parseInt(JOptionPane.showInputDialog("Distancia de " + nom
                + " en metros"));
        int tie = Integer.parseInt(JOptionPane.showInputDialog("Tiempo de " + nom
                + " en segundos"));
        g1.setNombre(nom);
        g1.setDistancia(dis);
        g1.setTiempo(tie);

        nom = JOptionPane.showInputDialog("Nombre del Garrobo #2");
        dis = Integer.parseInt(JOptionPane.showInputDialog("Distancia de " + nom
                + " en metros"));
        tie = Integer.parseInt(JOptionPane.showInputDialog("Tiempo de " + nom
                + " en segundos"));
        g2.setNombre(nom);
        g2.setDistancia(dis);
        g2.setTiempo(tie);

        String res = "Config. " + g1.getNombre() + " Dist. " + g1.getDistancia()
                + "m\tTiempo: " + g1.getTiempo() + "s\n";
        res += "Config. " + g2.getNombre() + " Dist. " + g2.getDistancia()
                + "m\tTiempo: " + g2.getTiempo() + "s\n";

        int disC = Integer.parseInt(JOptionPane.showInputDialog("Distancia a consultar(m)"));
        int tieC = Integer.parseInt(JOptionPane.showInputDialog("Tiempo a consultar(s)"));

        res += "\n " + g1.getNombre() + ", camina " + disC + "m  en " + g1.calcularTiempo(disC) + "s";
        res += "\n " + g2.getNombre() + ", camina " + disC + "m  en " + g2.calcularTiempo(disC) + "s";

        res += "\n " + g1.getNombre() + ", en " + tieC + "s  camina " + g1.calcularDistancia(tieC) + "m";
        res += "\n " + g2.getNombre() + ", en " + tieC + "s  camina " + g2.calcularDistancia(tieC) + "m";

        JOptionPane.showMessageDialog(null, res);
    }

}
