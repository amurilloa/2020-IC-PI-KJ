/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class ForMain {

    public static void main(String[] args) {
        //                                             x 
//        int[] arreglo = {10, 21, 34, 14, 35, 56, 67};
//        
//        for (int x : arreglo) {
//            System.out.println(x);
//        }
//
//        for (int i = 1; i <= 10; i++) {
//            if (i == 8) {
//                break;
//            }
//            System.out.println(i);
//        }
//
//        while (true) {
//            int y = Integer.parseInt(JOptionPane.showInputDialog("10"));
//            if (y == 10) {
//                break;
//            }
//            System.out.println(y);
//        }

        String txt = "";
        for (int i = 1; i <= 1000; i++) {
            if (i % 7 == 0) {
                txt += (i < 100 ? "  " : "") + i + (i % 70 == 0 ? "\n" : " ");
            }
        }
        JOptionPane.showMessageDialog(null, txt);
    }
}
