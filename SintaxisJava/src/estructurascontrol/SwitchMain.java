/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class SwitchMain {

    public static void main(String[] args) {
//        int dia = Integer.parseInt(JOptionPane.showInputDialog("Digite un día(1-7):"));
//        switch (dia) {
//            case 1:
//                System.out.println("Domingo");
//                break;
//            case 2:
//                System.out.println("Lunes");
//                break;
//            case 3:
//                System.out.println("Martes");
//                break;
//            case 4:
//                System.out.println("Miércoles");
//                break;
//            case 5:
//                System.out.println("Jueves");
//                break;
//            case 6:
//                System.out.println("Viernes");
//                break;
//            case 7:
//                System.out.println("Sábado");
//                break;
//        }
        //Pedir un día del 1 - 12 y retornan Enero, Febrero... 

//        int mes = Integer.parseInt(JOptionPane.showInputDialog("Digite un mes(1-12):"));
//
//        switch (mes) {
//            case 1:
//                JOptionPane.showMessageDialog(null, "Enero");
//                break;
//            case 2:
//                JOptionPane.showMessageDialog(null, "Febrero");
//                break;
//            case 3:
//                JOptionPane.showMessageDialog(null, "Marzo");
//                break;
//            case 4:
//                JOptionPane.showMessageDialog(null, "Abril");
//                break;
//            case 5:
//                JOptionPane.showMessageDialog(null, "Mayo");
//                break;
//            case 6:
//                JOptionPane.showMessageDialog(null, "Junio");
//                break;
//            case 7:
//                JOptionPane.showMessageDialog(null, "Julio");
//                break;
//            case 8:
//                JOptionPane.showMessageDialog(null, "Agosto");
//                break;
//            case 9:
//                JOptionPane.showMessageDialog(null, "Septiembre");
//                break;
//            case 10:
//                JOptionPane.showMessageDialog(null, "Octubre");
//                break;
//            case 11:
//                JOptionPane.showMessageDialog(null, "Noviembre");
//                break;
//            case 12:
//                JOptionPane.showMessageDialog(null, "Diciembre");
//                break;
//            default:
//                JOptionPane.showMessageDialog(null, "Número de mes inválido");
//        }
//        int cantidad = Integer.parseInt(JOptionPane.showInputDialog("1-5:"));
//
//        switch (cantidad) {
//            case 5:
//                System.out.println("************");
//            case 4:
//                System.out.println("************");
//            case 3:
//                System.out.println("************");
//            case 2:
//                System.out.println("************");
//            case 1:
//                System.out.println("************");
//        }
//        String letra = "x";
//        switch (letra) {
//
//            case "x":
//            case "X":
//                System.out.println("Algo");
//                break;
//            default:
//                System.out.println("Opción Inválida");
//        }
        String menu = "Calculadora - UTN v0.1\n"
                + "+  Sumar\n"
                + "-  Restar\n"
                + "*  Multiplicar\n"
                + "/  Dividir\n"
                + "Digite una opción";

        char op = JOptionPane.showInputDialog(menu).charAt(0);

       char x = '+';
        
        double n1 = Double.parseDouble(JOptionPane.showInputDialog("#1"));
        double n2 = Double.parseDouble(JOptionPane.showInputDialog("#2"));
        switch (op) {
            case '+':
                JOptionPane.showMessageDialog(null, n1 + "+" + n2 + "=" + (n1 + n2));
                break;
            case '-':
                JOptionPane.showMessageDialog(null, n1 + "-" + n2 + "=" + (n1 - n2));
                break;
            case '*':
                JOptionPane.showMessageDialog(null, n1 + "x" + n2 + "=" + (n1 * n2));
                break;
            case '/':
                JOptionPane.showMessageDialog(null, n1 + "/" + n2 + "=" + (n1 / n2));
                break;
            default:
                JOptionPane.showMessageDialog(null, "Opción Inválida");
        }

    }

    //Iteraciones: for(for-loop) y while(do while)
    //Imprimir con Formato
}
