/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class WhileMain {

    public static void main(String[] args) {
        int a;
//        int tabla = Integer.parseInt(JOptionPane.showInputDialog("Tabla"));
//        String txt = "";
//        int i = 1; // Inicio 
//        while (i <= 10) { //condición de parada
//            txt += tabla + "x" + i + "=" + tabla * i + "\n";
//            i++;
//        }
//        JOptionPane.showMessageDialog(null, txt);
        int total = 0;
        String sum = "";
        int i = 1;

        while (i <= 10) {
            int y = Integer.parseInt(JOptionPane.showInputDialog(i + "/10"));
            if (y < 0) {
                break;
            }
            total += y;
            sum += y + (i < 10 ? "+" : "");
            i++;
        }

        JOptionPane.showMessageDialog(null, sum + "=" + total);
    }
}
