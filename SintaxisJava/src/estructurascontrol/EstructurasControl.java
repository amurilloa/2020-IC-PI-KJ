/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class EstructurasControl {

    public static void main(String[] args) {

//        int x = 21;
//        int y = 19;
//
//        //Selección Simple
//        if (x == y) {
//            System.out.println("Son iguales");
//        }
//
//        //Seleccion doble
//        if (x == y) {
//            System.out.println("Son iguales");
//        } else {
//            System.out.println("Son diferentes");
//        }
//
//        if (x < y) {
//            System.out.println("La variable x=" + x + " es menor que la variable y=" + y);
//        } else if (x > y) {
//            System.out.println("La variable x=" + x + " es mayor que la variable y=" + y);
//        } else {
//            System.out.println("La variable x=" + x + " es igual que la variable y=" + y);
//        }
//
//        int canEle = 6;
//        int inicio = 1;
//
//        int ale = (int) (Math.random() * canEle) + inicio;
//        System.out.println(ale);
//
//        int num = ale;//Integer.parseInt(JOptionPane.showInputDialog("Digite un número: "));
//
//        String msj = "";
//        if (num >= 0) {
//            msj = "El número es positivo";
//        } else {
//            msj = "El número es negativo";
//        }
//
//        if (num % 5 == 0) {
//            msj += "\nEl número es divisible entre 5";
//        }
//
//        System.out.println(msj);//JOptionPane.showMessageDialog(null, msj);
//
//        int numPas = 5;//Integer.parseInt(JOptionPane.showInputDialog("Cant. de Pasajeros: "));
//        int numEjes = 2;// Integer.parseInt(JOptionPane.showInputDialog("Número de Ejes: "));
//        int costo = 10000;//Integer.parseInt(JOptionPane.showInputDialog("Costo: "));
//
//        double impBase = costo * 0.01;
//        double impPas = 0;
//        double impEjes = 0;
//
//        if (numPas < 20) {
//            impPas = impBase * 0.01;
//        } else if (numPas >= 20 && numPas <= 60) {
//            impPas = impBase * 0.05;
//        } else {
//            impPas = impBase * 0.08;
//        }
//
//        if (numEjes == 2) {
//            impEjes = impBase * 0.05;
//        } else if (numEjes == 3) {
//            impEjes = impBase * 0.10;
//        } else if (numEjes > 3) {
//            impEjes = impBase * 0.15;
//        }
//
//        double impuestos = impBase + impPas + impEjes;
//        double costoTotal = costo + impuestos;
//        System.out.println("Costo Total: " + costoTotal);
//        int x1 = 25;//Integer.parseInt(JOptionPane.showInputDialog("Digite un número: "));
//        if (x1 % 2 == 0) {
//            JOptionPane.showMessageDialog(null, "Variable x=" + x1 + " es par");
//        } else {
//            JOptionPane.showMessageDialog(null, "Variable x=" + x1 + " es impar");
//        }
//        int nota = Integer.parseInt(JOptionPane.showInputDialog("Digite una nota: "));
//
//        if (nota >= 90) {
//            JOptionPane.showMessageDialog(null, "Sobresaliente");
//        } else if (nota >= 80) {
//            JOptionPane.showMessageDialog(null, "Notable");
//        } else if (nota >= 70) {
//            JOptionPane.showMessageDialog(null, "Bien");
//        } else {
//            JOptionPane.showMessageDialog(null, "Insuficiente");
//        }
//
//        if (nota < 70) {
//            JOptionPane.showMessageDialog(null, "Insuficiente");
//        } else if (nota < 80) {
//            JOptionPane.showMessageDialog(null, "Bien");
//        } else if (nota < 90) {
//            JOptionPane.showMessageDialog(null, "Notable");
//        } else {
//            JOptionPane.showMessageDialog(null, "Sobresaliente");
//        }
//        int can = Integer.parseInt(JOptionPane.showInputDialog("Canciones: "));
//        int par = Integer.parseInt(JOptionPane.showInputDialog("Partituras: "));
//
//        if (can >= 7 && can <= 10) {
//            if (par == 0) {
//                JOptionPane.showMessageDialog(null, "Músico naciente");
//            } else if (par >= 1 && par <= 5) {
//                JOptionPane.showMessageDialog(null, "Músico estelar");
//            } else {
//                JOptionPane.showMessageDialog(null, "Músico en formación");
//            }
//        } else if (can > 10 && par > 5) {
//            JOptionPane.showMessageDialog(null, "Músico consagrado");
//        } else {
//            JOptionPane.showMessageDialog(null, "Músico en formación");
//        }
//        String detalle = "";
//        int monto = Integer.parseInt(JOptionPane.showInputDialog("Monto a retirar: "));
//
//        int moneda = 500;
//        if (monto >= moneda) {
//            int can = monto / moneda;
//            monto = monto % moneda;
//
//            String plural = can > 1 ? " billetes" : " billete";
//
////            String plurax = "";
////            if (can > 1) {
////                plurax = "billetes";
////            } else {
////                plurax = "billete";
////            }
//            detalle += can + plural + " de " + moneda + "\n";
//        }
//
//        moneda = 200;
//        if (monto >= moneda) {
//            int can = monto / moneda;
//            monto = monto % moneda;
//            String plural = can > 1 ? " billetes" : " billete";
//            detalle += can + plural + " de " + moneda + "\n";
//        }
//
//        moneda = 100;
//        if (monto >= moneda) {
//            int can = monto / moneda;
//            monto = monto % moneda;
//            String plural = can > 1 ? " billetes" : " billete";
//            detalle += can + plural + " de " + moneda + "\n";
//        }
//
//        moneda = 50;
//        if (monto >= moneda) {
//            int can = monto / moneda;
//            monto = monto % moneda;
//            String plural = can > 1 ? " billetes" : " billete";
//            detalle += can + plural + " de " + moneda + "\n";
//        }
//
//        moneda = 20;
//        if (monto >= moneda) {
//            int can = monto / moneda;
//            monto = monto % moneda;
//            String plural = can > 1 ? " billetes" : " billete";
//            detalle += can + plural + " de " + moneda + "\n";
//        }
//
//        moneda = 10;
//        if (monto >= moneda) {
//            int can = monto / moneda;
//            monto = monto % moneda;
//            String plural = can > 1 ? " billetes" : " billete";
//            detalle += can + plural + " de " + moneda + "\n";
//        }
//
//        moneda = 5;
//        if (monto >= moneda) {
//            int can = monto / moneda;
//            monto = monto % moneda;
//            String plural = can > 1 ? " billetes" : " billete";
//            detalle += can + plural + " de " + moneda + "\n";
//        }
//
//        moneda = 2;
//        if (monto >= moneda) {
//            int can = monto / moneda;
//            monto = monto % moneda;
//            String plural = can > 1 ? " monedas" : " moneda";
//            detalle += can + plural + " de " + moneda + "\n";
//        }
//
//        if (monto > 0) {
//            detalle += "1 moneda de 1";
//        }
//
//        JOptionPane.showMessageDialog(null, detalle);
        int precio = Integer.parseInt(JOptionPane.showInputDialog("Precio($): "));
        int viajeros = Integer.parseInt(JOptionPane.showInputDialog("Cantidad: "));

        if (viajeros != 0) {
            if (viajeros == 1) {
                int edad = Integer.parseInt(JOptionPane.showInputDialog("Edad: "));
                if (edad >= 18 && edad <= 30) {
                    double desc = precio * 0.078;
                    JOptionPane.showMessageDialog(null, "Desc. $" + desc
                            + "\nTotal: $" + (precio - desc));
                } else if (edad > 30) {
                    double desc = precio * 0.1;
                    JOptionPane.showMessageDialog(null, "Desc. $" + desc
                            + "\nTotal: $" + (precio - desc));
                } else {
                    JOptionPane.showMessageDialog(null, "Desc. $" + 0
                            + "\nTotal: $" + precio);
                }
            } else if (viajeros == 2) {
                double desc = 2 * precio * 0.115;
                JOptionPane.showMessageDialog(null, "Desc. $" + desc
                        + "\nTotal: $" + (2 * precio - desc));
            } else if (viajeros > 3) {
                double desc = viajeros * precio * 0.15;
                JOptionPane.showMessageDialog(null, "Desc. $" + desc
                        + "\nTotal: $" + (viajeros * precio - desc));
            } else {
                JOptionPane.showMessageDialog(null, "Desc. $" + 0
                        + "\nTotal: $" + precio * viajeros);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Favor digitar la cantidad correcta de viajeros");
        }

    }
}
