/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareaDos;

/**
 *
 * @author ALLAN
 */
public class OtroNumero {

    private int numero;

    public OtroNumero(int numero) {
        this.numero = numero;
    }

    public boolean esPrimo() {
        int con = 0;
        for (int i = 1; i <= numero; i++) {
            if (numero % i == 0) {
                con++;
            }
        }
        return con == 2;
    }

    public int tipo() {
        int sum = 0;
        for (int i = 1; i < numero; i++) {
            if (numero % i == 0) {
                sum += i;
            }
        }
        if (sum == numero) {
            return 1;
        } else if (sum < numero) {
            return 2;
        } else {
            return 3;
        }
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getNumero() {
        return numero;
    }

}
