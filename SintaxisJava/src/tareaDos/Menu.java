/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareaDos;

import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class Menu {

    public static void main(String[] args) {

        OtroNumero log = new OtroNumero(0);
        System.out.println(log.esPrimo());
        System.out.println(log.tipo());

        String menu = "Otro Número - UTN v0.1\n"
                + "1. Asignar un número\n"
                + "2. Primo/Compuesto\n"
                + "3. Tipo del Número\n"
                + "4. Salir";

        APP:
        while (true) {
            int op = Integer.parseInt(JOptionPane.showInputDialog(menu));
            switch (op) {
                case 1:
                    int num = Integer.parseInt(JOptionPane.showInputDialog("Número"));
                    log = new OtroNumero(num);
                    break;
                case 2:
                    String txt = log.esPrimo() ? " es primo" : " es compuesto";
                    JOptionPane.showMessageDialog(null, "El " + log.getNumero() + txt);
                    break;
                case 3:
                    txt = log.tipo() == 1 ? "perfecto" : log.tipo() == 2 ? "deficiente" : "abundante";
                    JOptionPane.showMessageDialog(null, "El " + log.getNumero() + " es " + txt);
                    break;
                case 4:
                    break APP;
            }
        }
    }

}
