/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author ALLAN
 */
public class LogicaEncuesta {

    private int total;
    private Encuesta[] encuestas;

    private final int HOMBRE = 1;
    private final int MUJER = 2;

    public LogicaEncuesta() {
        encuestas = new Encuesta[50];
    }

    public boolean resgistrar(Encuesta enc) {
        for (int i = 0; i < encuestas.length; i++) {
            if (encuestas[i] == null) {
                encuestas[i] = enc;
                total++;
                return true;
            }
        }
        return false;
    }

    public double porcentajeHom() {
        return porcentaje(HOMBRE);
    }

    public double porcentajeMuj() {
        return porcentaje(MUJER);
    }

    private double porcentaje(int sex) {
        double por = 0;
        for (Encuesta enc : encuestas) {
            if (enc != null && enc.getSexo() == sex) {
                por++;
            }
        }
        return 100 * por / total;
    }

    public String porcentajeTrabajan(int sex) {
        int th = 0;
        int tt = 0;
        double sht = 0;

        for (Encuesta enc : encuestas) {
            if (enc != null && enc.getSexo() == sex) {
                th++;
                if (enc.getTrabaja() == 1) {
                    tt++;
                    sht += enc.getSueldo();
                }
            }
        }

        double porHT = 100.0 * tt / th;
        double sueldoPro = sht / tt;

        String genero = sex == 1 ? "Hombres" : "Mujeres";
        
        return genero + " que Trabajan " + porHT + "%\n"
                + "Sueldo Promedio: " + sueldoPro;

    }

    public String imprimir() {
        String txt = "|";
        for (Encuesta enc : encuestas) {
            txt += " " + enc + " |";
        }
        return txt;
    }
}
