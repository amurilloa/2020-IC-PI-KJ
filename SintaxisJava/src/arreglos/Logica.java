/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author ALLAN
 */
public class Logica {

    public int[] generarArreglo(int tam) {
        int[] arr = new int[tam];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 10) + 1;
        }
        return arr;
    }

    public String imprimir(int[] arr) {
        String txt = "|";
        for (int i : arr) {
            txt += " " + i + " |";
        }
        return txt;
    }

    public double calcPromedio(int[] arr) {
        double pro = 0.0;
        for (int num : arr) {
            pro += num;
        }
        return pro / arr.length;
    }

    public int getMax(int[] arr) {
        int max = arr[0];//5

        for (int num : arr) {
            if (num > max) {
                max = num;
            }
        }
        return max;
    }

    public int getMin(int[] arr) {
        int min = arr[0];

        for (int num : arr) {
            if (num < min) {
                min = num;
            }
        }
        return min;
    }

    public boolean buscarElemento(int[] arr, int elemento) {

        for (int num : arr) {
            if (num == elemento) {
                return true;
            }
        }

        return false;
    }

    public int buscarElemento(int elemento, int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == elemento) {
                return i;
            }
        }

        return -1;
    }

    public int[] invertir(int[] arr) {
        int[] invertido = new int[arr.length];
        int pos = arr.length - 1;
        for (int num : arr) {
            invertido[pos--] = num;
        }
        return invertido;
    }

    public void correrDer(int[] arr) {
        int ult = arr[arr.length - 1];
        for (int i = arr.length - 2; i >= 0; i--) {
            arr[i + 1] = arr[i];
        }
        arr[0] = ult;
    }
}
