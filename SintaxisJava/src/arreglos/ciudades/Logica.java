/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos.ciudades;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Ciudad[] ciudades;

    public Logica() {
        ciudades = new Ciudad[10];
    }

    /**
     * Registra una ciudad en el primer espacio vacio del arreglo
     *
     * @param ciudad Elemento que se desea guardar en el arreglo
     */
    public void regitrar(Ciudad ciudad) {
        for (int i = 0; i < ciudades.length; i++) {
            if (ciudades[i] == null) {
                ciudades[i] = ciudad;
                break;
            }
        }
    }

    public String imprimir() {
        String msj = "| ";
        for (Ciudad ciudad : ciudades) {
            msj += ciudad + " | ";
        }
        return msj;
    }

    public String rutasDisp(int monto) {
        String res = "";
        for (Ciudad ciudad : ciudades) {
            if (ciudad != null && ciudad.getTarifa() <= monto) {
                res += ciudad + "\n";
            }
        }
        return res;
    }

    public Ciudad tarifaMayor() {
        Ciudad max = ciudades[0];
        
        for (Ciudad ciudad : ciudades) {
            if (ciudad != null && ciudad.getTarifa() >  max.getTarifa()) {
                max = ciudad;
            }
        }
        
        return max;
    }

}
