/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos.ciudades;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static void main(String[] args) {
        Logica log = new Logica();
        log.regitrar(new Ciudad("Alajuela", 2500));
        log.regitrar(new Ciudad("San Jose", 4500));
        log.regitrar(new Ciudad("Limón", 5500));
        log.regitrar(new Ciudad("Guanacaste", 3500));
        log.regitrar(new Ciudad("Heredia", 3700));
        log.regitrar(new Ciudad("Cartago", 4800));
        log.regitrar(new Ciudad("San Ramón", 2200));
        log.regitrar(new Ciudad("Tilarán", 2800));
        log.regitrar(new Ciudad("Fortuna", 1500));
//        log.regitrar(new Ciudad("Ciudad Quesada", 1800));
        //System.out.println(log.imprimir());
        System.out.println(log.rutasDisp(2200));
        System.out.println(log.tarifaMayor());
    }
}
