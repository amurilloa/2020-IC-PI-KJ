/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author ALLAN
 */
public class PracticaUno {

    public static void main(String[] args) {
        Logica log = new Logica();
        int[] arreglo = log.generarArreglo(5);
        System.out.println("Original: " + log.imprimir(arreglo));
        System.out.println("Promedio: " + log.calcPromedio(arreglo));
        System.out.println("Mayor: " + log.getMax(arreglo));
        System.out.println("Menor: " + log.getMin(arreglo));
        arreglo = log.invertir(arreglo);
        System.out.println("Invertido: " + log.imprimir(arreglo));
        log.correrDer(arreglo);
        System.out.println("Correr Derecha: " + log.imprimir(arreglo));
        
        
    }
}
