/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author ALLAN
 */
public class Arreglos {

    public static void main(String[] args) {
//        int[] numeros = new int[5];
//        int[] precios = {152, 548, 547, 845};
////        precios = new int[]{12, 2, 43, 4, 13, 324};
//
        String[] dias = {"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"};
        String[] meses = new String[12];
        meses[0] = "Enero";
        meses[1] = "Febrero";
        meses[2] = "Marzo";
        meses[3] = "Abril";
        meses[4] = "Mayo";
        
        meses[11] = "Diciembre";
        for (String mes : meses) {
            System.out.println(mes);
        }
        
        System.out.println(meses[4]);

//


//        for (int precio : precios) {
//            System.out.println(precio * 1.13);
//        }
//        
//        for (int i = 0; i < precios.length; i++) {
//            precios[i] *= 1.13;
//            System.out.println(precios[i]);
//        }
//
//        
//
//        int a = 12;
//        a = 16;
//
//        numeros[0] = 12;
//        numeros[3] = 5;
//
//        for (int i = 0; i < numeros.length; i++) {
//            numeros[i] = (int) (Math.random() * 10) + 1;
//        }
//
//        for (int i = 0; i < numeros.length; i++) {
//            System.out.print(numeros[i] + " | ");
//        }

        Logica log = new Logica();
        int[] temp = log.generarArreglo(5);
        System.out.println(log.imprimir(temp));

        Perro[] perros = new Perro[20];

        Perro p = new Perro("Capitán", "Blanco y Negro");
        perros[0] = p;
        perros[1] = new Perro("Charlie", "Cafe");
        perros[2] = perros[1];
        perros[1] = null;
        for (Perro perro : perros) {
            System.out.println(perro);
        }

    }
}
