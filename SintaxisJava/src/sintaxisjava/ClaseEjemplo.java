package sintaxisjava;

/**
 *
 * @author ALLAN
 */
public class ClaseEjemplo {

    //Atributos
    private String nombre;  //Variables globales
    private int fracturas;
    private int temp;
    private static int temp2;

    //Métodos
    public void metodo() {
        String temp = ""; //Variable local 
        fracturas = fracturas + this.temp;
    }

    public void metodoDos() {
        int temp = 2;
        fracturas = fracturas + this.temp + temp;
    }

    public void metodoTres(int temp) { //Parámetros  --> Variable loca
        fracturas = fracturas + this.temp + temp;
    }

    public static void metodoCuatro() { 
        int x = 10;
        temp2 = x;
        
    }

}
