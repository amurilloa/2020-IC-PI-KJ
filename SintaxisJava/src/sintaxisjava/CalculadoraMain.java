/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxisjava;

import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class CalculadoraMain {

    public static void main(String[] args) {

        Calculadora c = new Calculadora();

        double n1 = Double.parseDouble(JOptionPane.showInputDialog("#1"));
        double n2 = Double.parseDouble(JOptionPane.showInputDialog("#2"));
        double r = c.sumar(n1, n2);

        String txt = n1 + "+" + n2 + "=" + r;
        JOptionPane.showMessageDialog(null, txt);
    }
}
