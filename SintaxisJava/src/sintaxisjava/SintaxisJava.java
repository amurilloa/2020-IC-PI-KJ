/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxisjava;

/**
 *
 * @author Allan Murillo
 */
public class SintaxisJava {

    private int x;
    private static int y;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(y);
        System.out.println("Hola Mundo"); //Sirve para imprimir en la consola
        int a = 10; //Declarando una variable que se llama "a" y la inicializo con un valor 10
        double b = 1.9; //float y double 
        System.out.println(a);
        int c; //declaración de una variable
        c = 10;//inicialización de una variable 
        c = 13;//asignación de una variable
        c = a + 5;
        //Tipo de dato logico
        boolean bo1 = true;
        //Tipos de Datos Enteros
        byte b1 = 127;
        short s1 = 255;
        int i1 = 12312312; //Tipo de dato por defecto
        long l1 = 12342344234L;
        //Tipos de Datos Numericos con decimales
        float f1 = 1.21F;
        double d1 = 1.21; //Tipo de dato por defecto
        //Tipos de datos "texto"
        String str = "texto \u00A9\ntexto";
        // \n --> enter, \t --> tabulador, \\ --> escribe un backslash, \u0000 --> simbolo
        char c1 = 'a';
        //System.out.println(b1);
        char c2 = '\u00A9';
        System.out.println(str);
        System.out.println("\nEjemplo de Secuencias de Escape");
        System.out.println("CASO 1:\n" + "A\tB\tC\n1\t2\t3");
        System.out.println("CASO 2: " + "\"HOLA\"");
        System.out.println("CASO 3: " + "El caracter escape \"\\n\" cambia de línea");
        System.out.println("CASO 4: " + "El unicode \\u0040 vale \u0040");
        System.out.println("CASO 5: " + "El unicode \\u00D1 vale \u00D1");
        int a1 = 12;
        int aa = 0;
        int bb = 0;
        int cc = 0;
        final double PI = 3.1415926536;
        System.out.println((int) 129.123);
        System.out.println("Resultado de 35/4 = " + 35 / 4);
        System.out.println("Resultado de 35%4 = " + 35 % 4);
        System.out.println("Resultado de 35.0/4 = " + 35.0 / 4);

    }
}

// Comentario de una linea
/*
    Comentario en bloque 
    Comentario en bloque 
    Comentario en bloque 
 */

 /*
    adasd
    asdasdasd
    asdasd
    asdasd
    asda
    sd
 */
