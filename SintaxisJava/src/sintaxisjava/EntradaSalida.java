/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxisjava;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EntradaSalida {

//    nom = input("Digite un Nombre")
//    num = int(input("Digite un número"))
    public static void main(String[] args) {
        //6 DATOS 
//        System.out.println("Hola Mundo!!");
        Scanner sc = new Scanner(System.in);
//        System.out.print("Nombre: ");
//        String nombre = sc.nextLine();
//        System.out.println("Valor: " + nombre);
//        sc = new Scanner(System.in);
//
//        System.out.print("Digite un entero: ");
//        int n1 = Integer.parseInt(sc.nextLine());
//        System.out.print("Digite un double: ");
//        double d1 = Double.parseDouble(sc.nextLine());
//        System.out.print("Digite un boolean: ");
//        boolean b1 = Boolean.parseBoolean(sc.nextLine());
//        System.out.print("Digite un float: ");
//        float f1 = Float.parseFloat(sc.nextLine());

        System.out.print("Cédula: ");
        int ced = Integer.parseInt(sc.nextLine());
        System.out.print("Nombre: ");
        String nom = sc.nextLine();
        System.out.print("Edad: ");
        short edad = Short.parseShort(sc.nextLine());
        System.out.print("Sexo:\n F: Femenino\n M: Masculino\nSeleccione una opción: ");
        char sexo = sc.nextLine().charAt(0);
        System.out.print("Estatura(cm): ");
        float alto = Float.parseFloat(sc.nextLine());
        System.out.print("Peso(kg): ");
        double peso = Double.parseDouble(sc.nextLine());

        String txt = "Datos del Usuario\n"
                + "Ced. " + ced + "\n"
                + "Nombre: " + nom + "\n"
                + "Edad: " + edad + " Sexo: " + sexo + " \n"
                + "Estatura: " + alto + "cm Peso: " + peso + "kg\n";

        System.out.println(txt);

    }
}
