/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxisjava;

import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class EntradaSalidaJOP {

    public static int sumar(int num1, int num2) {
        int res = num1 + num2;
        return res;
    }

    public static void imprimir(String mensaje) {
        //System.out.println(mensaje);
        JOptionPane.showMessageDialog(null, mensaje, "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void main(String[] args) {

        int n1 = Integer.parseInt(JOptionPane.showInputDialog(null, "Número 1", "Sumar...",
                JOptionPane.QUESTION_MESSAGE));
        
        int n2 = Integer.parseInt(JOptionPane.showInputDialog(null, "Número 2", "Sumar...",
                JOptionPane.QUESTION_MESSAGE));

        int x = sumar(n1, n2);
        int x2 = sumar(n1, 14);
        int x3 = sumar(n1, 15);
        imprimir("Resultado: " + x);

//        JOptionPane.showMessageDialog(null, "Mensaje 1");
//        JOptionPane.showMessageDialog(null, "Mensaje 2", "Titulo", JOptionPane.INFORMATION_MESSAGE);
        String nom = JOptionPane.showInputDialog("Nombre");
        int ced = Integer.parseInt(JOptionPane.showInputDialog(null, "Cédula", "Registro de ...",
                JOptionPane.QUESTION_MESSAGE));

        short edad = Short.parseShort(JOptionPane.showInputDialog(null, "Edad", "Registro de ...",
                JOptionPane.QUESTION_MESSAGE));
        char sexo = JOptionPane.showInputDialog(null, "Sexo:\n F: Femenino\n M: Masculino\nSeleccione una opción:", "Registro de ...",
                JOptionPane.QUESTION_MESSAGE).charAt(0);
        float alto = Float.parseFloat(JOptionPane.showInputDialog(null, "Estatura(cm)", "Registro de ...",
                JOptionPane.QUESTION_MESSAGE));
        double peso = Double.parseDouble(JOptionPane.showInputDialog(null, "Peso(kg)", "Registro de ...",
                JOptionPane.QUESTION_MESSAGE));

        String txt = "Datos del Usuario\n"
                + "Ced. " + ced + "\n"
                + "Nombre: " + nom + "\n"
                + "Edad: " + edad + "\n"
                + "Sexo: " + sexo + " \n"
                + "Estatura: " + alto + "cm" + "\n"
                + "Peso: " + peso + "kg\n";

        JOptionPane.showMessageDialog(null, txt, "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null, txt, "PREGUNTA", JOptionPane.QUESTION_MESSAGE);
        JOptionPane.showMessageDialog(null, txt, "ALERTA", JOptionPane.WARNING_MESSAGE);
        JOptionPane.showMessageDialog(null, txt, "ERROR", JOptionPane.ERROR_MESSAGE);
        JOptionPane.showMessageDialog(null, txt, "SIN ICONO", JOptionPane.PLAIN_MESSAGE);
    }
}
