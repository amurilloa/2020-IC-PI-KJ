/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.banco;

/**
 *
 * @author amurilloa
 */
public class Logica {

    private final Cliente[] clientes;
    private final Cuenta cuentaUno;
    private final Cuenta cuentaDos;
    private String txt;

    public Logica() {
        clientes = new Cliente[10];
        cuentaUno = new Cuenta(10000);
        cuentaDos = new Cuenta(20000);

    }

    /**
     * Registra un cliente
     *
     * @param cli Cliente que se desea registrar
     * @return true si el cliente se registra con éxito, false caso contrario
     */
    public boolean registrar(Cliente cli) {
        for (int i = 0; i < clientes.length; i++) {
            if (clientes[i] == null) {
                clientes[i] = cli;
                txt = "Cliente: " + cli.getNombre() + "\n";
                txt += "Cédula: " + cli.getCedula() + "\n\nDetalle de Transacciones:\n";
                return true;
            }
        }
        return false;
    }

    public String getClientes() {
        String txt = "Lista de Clientes\n";
        int num = 1;
        for (Cliente cliente : clientes) {
            if (cliente != null) {
                txt += num + ". " + cliente.getCedula() + " - " + cliente.getNombre() + "\n";
            } else {
                txt += num + ". _____________________\n";
            }
            num++;
        }
        return txt;
    }

    public Cliente existe(String ced) {
        for (Cliente cliente : clientes) {
            if (cliente != null && cliente.getCedula().equals(ced)) {
                txt = "Cliente: " + cliente.getNombre() + "\n";
                txt += "Cédula: " + cliente.getCedula() + "\n\nDetalle de Transacciones:\n";
                return cliente;
            }
        }
        return null;
    }

    public double consultar(int num) {
        if (num == 1) {
            double sal = cuentaUno.getSaldo();
            txt += "Saldo de Cuenta #1: ₡" + sal + "\n";
            return sal;
        } else {
            double sal = cuentaDos.getSaldo();
            txt += "Saldo de Cuenta #2: ₡" + sal + "\n";
            return sal;
        }
    }

    public double retirar(int num, double monto) {
        if (num == 1) {
            double ret = cuentaUno.retirar(monto);
            String temp = ret == 0 ? "Retiro de la Cuenta #1: Fondos Insuficientes" : "Retiro de la Cuenta #1: ₡" + monto;
            txt += temp + "\n";
            return ret;
        } else {
            double ret = cuentaDos.retirar(monto);
            String temp = ret == 0 ? "Retiro de la Cuenta #2: Fondos Insuficientes" : "Retiro de la Cuenta #2: ₡" + monto;
            txt += temp + "\n";
            return ret;
        }
    }

    public void depositar(int num, double monto) {
        txt += "Depósito en Cuenta #" + num + ": ₡" + monto + "\n";
        if (num == 1) {
            cuentaUno.abonar(monto);
        } else {
            cuentaDos.abonar(monto);
        }

    }

    public String comprobante() {
        return txt;
    }

}
