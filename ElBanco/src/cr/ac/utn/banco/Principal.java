/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.banco;

import javax.swing.JOptionPane;

/**
 *
 * @author amurilloa
 */
public class Principal {

    public static void main(String[] args) {
        Logica log = new Logica();
        log.registrar(new Cliente("206470762", "Allan Murilo Alfaro"));
        log.registrar(new Cliente("203440745", "Luis López Rueda"));
        log.registrar(new Cliente("105370662", "Juan Mora Porras"));

        String menuU = "1. Iniciar Sesión\n"
                + "2. Salir";

        String menuD = "1. Consultar Saldos\n"
                + "2. Depositar\n"
                + "3. Retirar\n"
                + "4. Salir";

        String[] menuDD = {"1. Consultar Saldos", "2. Depositar", "3. Retirar", "4. Salir"};
        String[] cuentas = {"Cuenta 1", "Cuenta 2"};

        APP:
        while (true) {
            int op = Util.leerInt(menuU); 
           switch (op) {
                case -1:
                    Util.mostrar(log.getClientes());
                    break;
                case 1:
                    String ced = Util.leer("Cédula");
                    Cliente cli = log.existe(ced);
                    if (cli == null) {
                        if (Util.confirmar("¿Desea Registrarse?")) {
                            String nom = Util.leer("Nombre:");
                            Cliente cliNuevo = new Cliente(ced, nom);
                            if (!log.registrar(cliNuevo)) {
                                Util.error("No se pudo registrar el usuario, favor intente nuevamente");
                                break;
                            }
                        } else {
                            break;
                        }
                    }

                    SES:
                    while (true) {
                        op = Util.menuOpciones(menuDD, "Seleccione una opción:") + 1;
                        switch (op) {
                            case 1:
                                int num = Util.menuOpciones(cuentas, "Seleccione una cuenta: ") + 1;
                                Util.mostrar("Saldo de Cuenta # " + num + ": " + log.consultar(num));
                                break;
                            case 2:
                                num = Util.leerInt("Cuenta # (1-2)");
                                double monto = Util.leerDouble("Monto a depositar:");
                                log.depositar(num, monto);
                                break;
                            case 3:
                                num = Util.leerInt("Cuenta # (1-2)");
                                monto = Util.leerDouble("Monto a retirar:");
                                double ret = log.retirar(num, monto);
                                String txt = ret == 0 ? "Fondos Insuficientes" : "Monto Retirado:" + ret;
                                Util.mostrar("Cuenta # " + num + ": " + txt);
                                break;
                            case 0:
                            case 4:
                                Util.mostrar(log.comprobante());
                                break SES;
                        }
                    }
                    break;
                case 2:
                    break APP;
            }
        }

    }
}
