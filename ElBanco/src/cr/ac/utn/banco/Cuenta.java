/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.banco;

/**
 *
 * @author amurilloa
 */
public class Cuenta {

    private double saldo;

    public Cuenta(double saldoInicial) {
        if (saldoInicial > 0) {
            saldo = saldoInicial;
        }
    }

    /**
     * Método que suma al saldo de la cuenta
     *
     * @param monto double a sumar en el saldo de la cuenta
     */
    public void abonar(double monto) {
        saldo += monto;
    }

    /**
     * Método que resta al saldo de la cuenta
     * @param monto double monto a retirar de la cuenta
     * @return double monto si hay fondos suficientes o 0 en caso contrario
     */
    public double retirar(double monto) {
        if (saldo >= monto) {
            saldo -= monto;
            return monto;
        } else {
            return 0;
        }
    }

    public double getSaldo() {
        return saldo;
    }

    @Override
    public String toString() {
        return "Cuenta{" + "saldo=" + saldo + '}';
    }
     
}
