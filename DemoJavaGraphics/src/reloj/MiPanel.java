/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reloj;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author amurilloa
 */
public class MiPanel extends JPanel {

    private Numero num;    
    private Teclado key;
    
    public MiPanel() {
        setBackground(new Color(4,36,99));
        num = new Numero(350, 280, Color.WHITE, 12);
        key = new Teclado(num);
        addKeyListener(key);
        setFocusable(true);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1000, 720);
    }

    @Override
    public void paint(Graphics gg) {
        super.paint(gg);
        Graphics2D g = (Graphics2D) gg;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        num.pintar(g);

        int hor = getHeight() / 2;
        int ver = getWidth() / 2;

        //Guias
        g.setColor(Color.WHITE);

        //g.drawLine(0, hor, getWidth(), hor);
        g.drawLine(ver, 0, ver, getHeight());
    }
}

/**
 * Clase Numero: x, y, numero, color1, color2, modo Presionan los números del 0
 * al 9 --> 1
 */
