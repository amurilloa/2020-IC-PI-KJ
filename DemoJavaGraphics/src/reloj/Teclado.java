/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reloj;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class Teclado implements KeyListener {

    private Numero num;
 
    public Teclado() {

    }

    public Teclado(Numero num) {
        this.num = num;
    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        
        if (num != null && e.getKeyCode() >= KeyEvent.VK_0 && e.getKeyCode() <= KeyEvent.VK_9) {
            num.setValor(e.getKeyCode() - 48);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
