/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reloj;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import javax.swing.JPanel;

/**
 *
 * @author amurilloa
 */
public class MiPanelReloj extends JPanel implements KeyListener {

    private int ms;
    private final LinkedList<Numero> numeros;
    private int arco;
    private boolean inicio;
    private String tiempo;
    private int con;

    private LinkedList<String> tiempos;

    public MiPanelReloj() {
        setBackground(new Color(4, 36, 99));
        numeros = new LinkedList<>();
        numeros.add(new Numero(178, 280, Color.WHITE, 0));
        numeros.add(new Numero(278, 280, Color.WHITE, 0));

        numeros.add(new Numero(408, 280, Color.WHITE, 0));
        numeros.add(new Numero(508, 280, Color.WHITE, 0));

        numeros.add(new Numero(638, 280, Color.WHITE, 0));
        numeros.add(new Numero(738, 280, Color.WHITE, 0));

        tiempos = new LinkedList<>();
        con = 1;
        addKeyListener(this);
        setFocusable(true);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1000, 720);
    }

    @Override
    public void paint(Graphics gg) {
        super.paint(gg);
        Graphics2D g = (Graphics2D) gg;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        if (inicio) {
            ms += 25;
            conTiempo();
        }

        g.setStroke(new BasicStroke(15));
        g.setColor(Color.BLUE);
        g.drawArc(125, 35, 750, 650, 90, -arco);

        for (Numero numero : numeros) {
            numero.pintar(g);
        }

        int y = 480;
        for (String tiempo : tiempos) {
            g.setFont(new Font("DUMMY", Font.PLAIN, 28));
            g.setColor(Color.WHITE);
            g.drawString(tiempo, 360, y);
            y += 40;
        }

        g.fillOval(380, 320, 10, 10);
        g.fillOval(380, 390, 10, 10);

        g.fillOval(610, 320, 10, 10);
        g.fillOval(610, 390, 10, 10);

        int hor = getHeight() / 2;
        int ver = getWidth() / 2;

        //Guias
        g.setColor(Color.WHITE);

        //g.drawLine(0, hor, getWidth(), hor);
        //g.drawLine(ver, 0, ver, getHeight());
    }

    private void conTiempo() {
        //ms -> ss -> mm -> hh;
        int totSeg = ms / 1000;

        int totMin = totSeg / 60;
        int seg = totSeg % 60;

        //Llenar arco
        //arco = 360*(seg*1000+(ms%1000))/60000;
        arco = (360 * seg / 60);

        int hrs = totMin / 60;
        int min = totMin % 60;

        this.tiempo = String.format("%02d:%02d:%02d", hrs, min, seg);

        String tiempo = String.format("%02d%02d%02d", hrs, min, seg);
        for (int i = 0; i < numeros.size(); i++) {
            numeros.get(i).setValor(Integer.parseInt(String.valueOf(tiempo.charAt(i))));
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_L) {
            if (tiempos.size() >= 5) {
                tiempos.removeFirst();
            }
            tiempos.add("LAP " + con++ + " --> " + tiempo);
        }
        if (e.getKeyCode() == KeyEvent.VK_S) {
            inicio = true;
        }
        if (e.getKeyCode() == KeyEvent.VK_R) {
            inicio = false;
            tiempos.clear();
            ms = 0;
            for (int i = 0; i < numeros.size(); i++) {
                numeros.get(i).setValor(0);
            }
            arco = 0;
            con = 1;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}

/**
 * Clase Numero: x, y, numero, color1, color2, modo Presionan los números del 0
 * al 9 --> 1
 */
