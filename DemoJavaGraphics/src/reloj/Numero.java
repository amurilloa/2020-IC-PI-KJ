/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reloj;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author amurilloa
 */
public class Numero {

    private final int x;
    private final int y;
    private final Color color;
    private int valor;

    public Numero(int x, int y, Color color, int valor) {
        this.x = x;
        this.y = y;
        this.color = color;
        setValor(valor);
    }

    public final void setValor(int valor) {
        if (valor >= 0 && valor <= 9) {
            this.valor = valor;
        }
    }

    public void pintar(Graphics2D g) {

        int[] arr1 = {5, 10, 70, 75, 70, 10};
        int[] arr2 = {5, 0, 0, 5, 10, 10};

        g.setColor(color);

        if (valor != 1 && valor != 4) {
            g.fillPolygon(inc(arr1, x + 2), inc(arr2, y), arr1.length); // Arriba
        }

        if (valor != 1 && valor != 2 && valor != 3 && valor != 7) {
            g.fillPolygon(inc(arr2, x), inc(arr1, y + 2), arr2.length); // Izq-Arr
        }
        if (valor != 5 && valor != 6) {
            g.fillPolygon(inc(arr2, x + 74), inc(arr1, y + 2), arr2.length); // Der-Arr
        }
        if (valor != 0 && valor != 1 && valor != 7) {
            g.fillPolygon(inc(arr1, x + 2), inc(arr2, y + 74), arr1.length); // Centro
        }

        if (valor != 1 && valor != 3 && valor != 4 && valor != 5 && valor != 7 && valor != 9) {
            g.fillPolygon(inc(arr2, x), inc(arr1, y + 76), arr2.length); // Izq-Aba
        }
        if (valor != 2) {
            g.fillPolygon(inc(arr2, x + 74), inc(arr1, y + 76), arr2.length); // Der-Abj
        }
        if (valor != 1 && valor != 4 && valor != 7) {
            g.fillPolygon(inc(arr1, x + 2), inc(arr2, y + 148), arr1.length); // Abj
        }
    }

    private int[] inc(int[] arr, int valor) {
        int[] temp = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            temp[i] = arr[i] + valor;
        }
        return temp;
    }
}
