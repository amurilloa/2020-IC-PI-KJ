/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reloj;

import java.awt.BorderLayout;
import javax.swing.JFrame;

/**
 *
 * @author amurilloa
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        JFrame frm = new JFrame("DemoGraphics");
        frm.setLayout(new BorderLayout());
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MiPanelReloj p = new MiPanelReloj();  
        //PanelEjemplo p = new PanelEjemplo();
        frm.add(p, BorderLayout.CENTER);
        frm.pack();
        frm.setLocationRelativeTo(null);
        //frm.setExtendedState(Frame.MAXIMIZED_BOTH);
        frm.setVisible(true);
        
        while (true) {
            frm.repaint();
            Thread.sleep(25);
        }
        
    }
    
}
