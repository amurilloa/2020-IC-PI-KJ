/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaminas;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author amurilloa
 */
public class Cuadrito {

    private int numero;
    private boolean mina;
    private boolean flag;
    private boolean exploto;
    private boolean select;

    private final int x;
    private final int y;
    private final int size;
    private final Color color;
    private final Color colorM;

    private final String PATH_F = "img/flag_icon.png";
    private final Color COLOR_S = new Color(0, 237, 124, 100);

    public Cuadrito(int x, int y, Color color, int size) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.size = size;
        colorM = colorAleatorio();
    }

    public void pintar(Graphics2D g) {

        if (exploto) {
            g.setColor(new Color(255, 255, 216));
            g.fillRect(x, y, size, size);
            g.setColor(new Color(0, 0, 0, 50));
            g.drawRect(x, y, size, size);

        } else {
            g.setColor(select ? COLOR_S : color);
            g.fillRect(x, y, size, size);
        }
        if (flag) {
            try {
                g.drawImage(ImageIO.read(new File(PATH_F)), x + 3, y + 4, null);
            } catch (IOException ex) {
                System.out.println("No encontró la bandera");
            }
        }
        if (mina && exploto) {
            g.setColor(colorM);
            g.fillOval(x + 5, y + 5, size - 10, size - 10);
        }
        if (flag && exploto && !mina) {
            g.setColor(new Color(255, 0, 0, 200));
            g.setFont(new Font("Arial", Font.BOLD, 50));
            g.drawString("X", x + 5, y + 38);
        }

        if (exploto && numero > 0) {
            g.setColor(Color.BLUE);
            g.setFont(new Font("Arial", Font.BOLD, 22));
            g.drawString(String.valueOf(numero), x + 12, y + 28);
        }

    }

    private Color colorAleatorio() {
        int r = (int) (Math.random() * 255);
        int g = (int) (Math.random() * 255);
        int b = (int) (Math.random() * 255);
        return new Color(r, g, b);
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setMina(boolean mina) {
        this.mina = mina;
    }

    public int getNumero() {
        return numero;
    }

    public boolean isMina() {
        return mina;
    }

    public boolean isFlag() {
        return flag;
    }

    public boolean isExploto() {
        return exploto;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public void setExploto(boolean exploto) {
        this.exploto = exploto;
    }

}
