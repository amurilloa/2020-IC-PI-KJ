/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaminas;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 *
 * @author amurilloa
 */
public class Mouse implements MouseListener, MouseMotionListener {

    private int x;
    private int y;
    private Buscaminas juego;

    public Mouse(Buscaminas juego) {
        this.juego = juego;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        juego.click(e.getButton());
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        x = e.getX();
        y = e.getY();
        juego.setCursor(x,y);
    }

}
