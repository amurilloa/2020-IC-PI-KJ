/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaminas;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author amurilloa
 */
public class Buscaminas {

    private int x;
    private int y;
    private int seg;
    private int bombas;
    private int mf;
    private int mc;

    private Cuadrito[][] tablero;

    private final Color COLOR_T = new Color(0, 122, 64);
    private final Color COLOR_P = new Color(0, 158, 84);
    private final Color COLOR_S = new Color(0, 187, 99);
    public final int SIZE = 40;

    public Buscaminas(int y) {
        this.y = y;
        bombas = 10;
        generarTablero();
    }

    public void pintar(Graphics2D g) {
        //Titulo 
        g.setColor(COLOR_T);
        g.fillRect(x, y - SIZE, SIZE * 10, SIZE);

        //Pinta el tablero
        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {
                tablero[f][c].pintar(g);
            }
        }
    }

    public void setCursor(int x, int y) {
        int mfb = mf;
        int mcb = mc;

        mf = (y - SIZE) / SIZE;
        mc = x / SIZE;
        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {
                tablero[f][c].setSelect(false);
            }
        }
        if (mf >= 0 && mf <= 7 && mc >= 0 && mc <= 9) {
            tablero[mf][mc].setSelect(true);
        } else {
            mf = mfb;
            mc = mcb;
        }
    }

    private void generarTablero() {

        //Crea el tablero
        tablero = new Cuadrito[8][10];
        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {
                boolean iguales = (f % 2 == 0 && c % 2 == 0) || (f % 2 != 0 && c % 2 != 0);
                Color color = iguales ? COLOR_S : COLOR_P;
                tablero[f][c] = new Cuadrito(x + (c * SIZE), y + (f * SIZE), color, SIZE);
            }
        }
        //Coloca las bombas
        int col = bombas;
        while (col > 0) {
            int f = (int) (Math.random() * 8);
            int c = (int) (Math.random() * 10);
            if (!tablero[f][c].isMina()) {
                tablero[f][c].setMina(true);
                col--;
            }
        }

        //Colocar los números
        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {
                if (!tablero[f][c].isMina()) {
                    int con = 0;
                    if (f > 0 && c > 0 && tablero[f - 1][c - 1].isMina()) {
                        con++;
                    }
                    if (f > 0 && tablero[f - 1][c].isMina()) {
                        con++;
                    }
                    if (f > 0 && c < 9 && tablero[f - 1][c + 1].isMina()) {
                        con++;
                    }
                    if (c > 0 && tablero[f][c - 1].isMina()) {
                        con++;
                    }
                    if (c < 9 && tablero[f][c + 1].isMina()) {
                        con++;
                    }
                    if (f < 7 && c > 0 && tablero[f + 1][c - 1].isMina()) {
                        con++;
                    }
                    if (f < 7 && tablero[f + 1][c].isMina()) {
                        con++;
                    }
                    if (f < 7 && c < 9 && tablero[f + 1][c + 1].isMina()) {
                        con++;
                    }
                    tablero[f][c].setNumero(con);
                }
            }
        }

    }

    public void click(int button) {
        if (button == 1 && !tablero[mf][mc].isFlag()) {
            if (tablero[mf][mc].getNumero() == 0) {
                explosion(mf, mc);
            } else {
                tablero[mf][mc].setExploto(true);
            }
        } else if (button == 3 && !tablero[mf][mc].isExploto()) {
            tablero[mf][mc].setFlag(!tablero[mf][mc].isFlag());
        }
    }

    private void explosion(int f, int c) {
        if (tablero[f][c].isExploto()) {
            return;
        }
        tablero[f][c].setExploto(true);
        if (f > 0 && c > 0 && !tablero[f - 1][c - 1].isMina()
                && tablero[f - 1][c - 1].getNumero() == 0) {
            explosion(f - 1,c - 1);
        }
        if (f > 0 && !tablero[f - 1][c].isMina()
                && tablero[f - 1][c].getNumero() == 0) {
            explosion(f - 1,c);
        }
        if (f > 0 && c < 9 && !tablero[f - 1][c + 1].isMina()
                && tablero[f - 1][c + 1].getNumero() == 0) {
            explosion(f - 1,c + 1);
        }
        if (c > 0 && tablero[f][c - 1].getNumero() == 0) {
            explosion(f,c - 1);
        }
        if (c < 9 && !tablero[f][c + 1].isMina() && tablero[f][c + 1].getNumero() == 0) {
            explosion(f ,c + 1);
        }
        if (f < 7 && c > 0 && !tablero[f + 1][c - 1].isMina()
                && tablero[f + 1][c - 1].getNumero() == 0) {
            explosion(f + 1,c - 1);
        }
        if (f < 7 && !tablero[f + 1][c].isMina() && tablero[f + 1][c].getNumero() == 0) {
            explosion(f + 1,c );
        }
        if (f < 7 && c < 9 && !tablero[f + 1][c + 1].isMina()
                && tablero[f + 1][c + 1].getNumero() == 0) {
            explosion(f + 1,c + 1);

        }
    }

}
