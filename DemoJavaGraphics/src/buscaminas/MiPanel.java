/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaminas;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author amurilloa
 */
public class MiPanel extends JPanel {
    
    private Buscaminas juego;
    private Mouse raton;
    
    public MiPanel() {
        setBackground(Color.WHITE);
        setFocusable(true);
        juego = new Buscaminas(40);
        raton = new Mouse(juego);
        addMouseListener(raton);
        addMouseMotionListener(raton);
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(400, 360);
    }
    
    @Override
    public void paint(Graphics gg) {
        super.paint(gg);
        Graphics2D g = (Graphics2D) gg;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        
        juego.pintar(g);
    }
    
}
