/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demojavagraphics;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author amurilloa
 */
public class Key implements KeyListener {
    
    private boolean inicio;
    private boolean pausa;
    private Bola bola;
    
    public Key(Bola bola) {
        this.bola = bola;
    }
    
    public void setBola(Bola bola) {
        this.bola = bola;
    }
    
    public boolean isInicio() {
        return inicio;
    }

    public boolean isPausa() {
        return pausa;
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
        System.out.println(e.getKeyCode());
        
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_I) {
            inicio = true;
            int ra = (int) (Math.random() * 8) + 1;
            bola.setDir(ra);
        } else if (inicio && e.getKeyCode() == KeyEvent.VK_SPACE) {
            pausa=!pausa;
        }
        
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
    }
    
}
