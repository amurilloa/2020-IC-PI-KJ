/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demojavagraphics;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author amurilloa
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        JFrame frm = new JFrame("DemoGraphics");
        frm.setLayout(new BorderLayout());
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MiPanel p = new MiPanel();
        frm.add(new PanelTitulo(), BorderLayout.NORTH);        
        frm.add(new MiPanel(), BorderLayout.CENTER);
        frm.add(new PanelPie(), BorderLayout.SOUTH);
        frm.setLocationRelativeTo(null);
        frm.pack();
        //frm.setExtendedState(Frame.MAXIMIZED_BOTH);
        frm.setVisible(true);
        
        while (true) {
            frm.repaint();
            Thread.sleep(50);
        }
        
    }
    
}
