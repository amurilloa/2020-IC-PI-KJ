/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demojavagraphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.LinkedList;
import javax.swing.JPanel;

/**
 *
 * @author amurilloa
 */
public class MiPanel extends JPanel {

    private final Key teclado;
    private final Mouse raton;
    private final Bola bola;
    private final LinkedList<Bola> bolas;

    public MiPanel() {
        setBackground(Color.WHITE);
        bola = new Bola(695, 280, Color.LIGHT_GRAY);
        bolas = new LinkedList<>();
        bolas.add(new Bola(100, 100, Color.YELLOW));
        bolas.add(new Bola(200, 150, Color.PINK));
        bolas.add(new Bola(500, 550, Color.CYAN));
        teclado = new Key(bola);
        raton = new Mouse();
        addKeyListener(teclado);
        addMouseListener(raton);
        addMouseMotionListener(raton);
        setFocusable(true);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1280, 720);
    }

    @Override
    public void paint(Graphics gg) {
        super.paint(gg);
        Graphics2D g = (Graphics2D) gg;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        if (teclado.isInicio()) {
            bola.setColor(Color.RED);
            for (Bola b : bolas) {
                b.pintar(g);
                b.mover(getWidth(), getHeight());
            }
            if (!teclado.isPausa()) {
                bola.mover(getWidth(), getHeight());
                for (Bola b : bolas) {
                    if (bola.isChocando(b)) {
                        Bola nueva = new Bola(b.getColor());
                        nueva.generarXY(getWidth(), getHeight());
                        b.generarXY(getWidth(), getHeight());
                        b.setDir(1);
                        bolas.add(nueva);
                    }
                }
            } else {
                g.setFont(new Font("sansserif", Font.BOLD, 18));
                g.setColor(Color.LIGHT_GRAY);
                g.fillRect(650, 250, 80, 80);
                g.setColor(Color.WHITE);
                g.drawString("PAUSA", 670, 305);
            }
        }
        bola.pintar(g);
        if (!teclado.isInicio()) {
            g.setFont(new Font("sansserif", Font.BOLD, 18));
            g.setColor(Color.LIGHT_GRAY);
            g.fillRect(650, 250, 150, 150);
            g.setColor(Color.WHITE);
            g.drawString("I - Iniciar", 670, 305);
            g.drawString("E - Salir", 670, 355);
        }
        g.setColor(Color.RED);
        int hor = getHeight() / 2;
        int ver = getWidth() / 2;
        g.fillOval(raton.getX(), raton.getY(), 5, 5);

        g.drawLine(ver, getHeight(), raton.getX(), raton.getY());

        //Guias
        g.setColor(Color.RED);

        g.drawLine(0, hor, getWidth(), hor);
        g.drawLine(ver, 0, ver, getHeight());
    }

}
