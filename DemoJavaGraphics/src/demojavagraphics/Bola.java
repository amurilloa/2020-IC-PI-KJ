/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demojavagraphics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 *
 * @author amurilloa
 */
public class Bola {

    private int x;
    private int y;
    private Color color;
    private int dir;
    private final int TAM = 50;
    private final int SPD = 4;

    public Bola(Color color) {
        this.color = color;
    }

    public Bola(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public void pintar(Graphics2D g) {
        g.setColor(color);
        g.fillOval(x, y, TAM, TAM);
        g.setFont(new Font("sansserif", Font.BOLD, 8));
        g.setColor(Color.BLACK);
        g.drawString(String.format("x=%d,y=%d", x, y), x - 4, y + TAM + 8);
        //TODO: Guía del bounds o area de choque, se puede eliminar en cualquier momento
        g.setColor(Color.BLACK);
        g.drawRect(x + 5, y + 5, TAM - 10, TAM - 10);
    }

    public Rectangle getBounds() {
        return new Rectangle(x + 5, y + 5, TAM - 10, TAM - 10);
    }

    public boolean isChocando(Bola b) {
        return this.getBounds().intersects(b.getBounds());
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public Color getColor() {
        return color;
    }

    public void generarXY(int ancho, int alto) {
        x = (int) (Math.random() * (ancho - TAM));
        y = (int) (Math.random() * (alto - TAM));
    }

    public void mover(int ancho, int alto) {
        if (dir == 1) { //Arriba
            y -= SPD;
            if (y <= 0) {
                dir = generarPos();
            }
        } else if (dir == 2) { //Arr+Der
            x += SPD;
            y -= SPD;
            if (x + TAM >= ancho) {
                dir = generarPos(true);
            } else if (y <= 0) {
                dir = generarPos(false);
            }
        } else if (dir == 3) { //Derecha
            x += SPD;
            if (x + TAM >= ancho) {
                dir = generarPos();
            }
        } else if (dir == 4) { //Abj+Der
            x += SPD;
            y += SPD;
            if (x + TAM >= ancho) {
                dir = generarPos(true);
            } else if (y + TAM >= alto) {
                dir = generarPos(false);
            }

        } else if (dir == 5) { //Abajo
            y += SPD;
            if (y + TAM >= alto) {
                dir = generarPos();
            }
        } else if (dir == 6) { // Aba+Izq
            x -= SPD;
            y += SPD;
            if (x <= 0) {
                dir = generarPos(true);
            } else if (y + TAM >= alto) {
                dir = generarPos(false);
            }
        } else if (dir == 7) { //Izquierda
            x -= SPD;
            if (x <= 0) {
                dir = generarPos();
            }
        } else if (dir == 8) { //Arr+Iz
            x -= SPD;
            y -= SPD;
            if (x <= 0) {
                dir = generarPos(true);
            } else if (y <= 0) {
                dir = generarPos(false);
            }
        }
    }

    private int generarPos(boolean costado) {
        int ra = (int) (Math.random() * 2);
        if (dir == 2) {
            int[] pos = costado ? new int[]{7, 8} : new int[]{5, 4};
            return pos[ra];
        } else if (dir == 4) {
            int[] pos = costado ? new int[]{6, 7} : new int[]{1, 2};
            return pos[ra];
        } else if (dir == 6) {
            int[] pos = costado ? new int[]{3, 4} : new int[]{8, 1};
            return pos[ra];
        } else if (dir == 8) {
            int[] pos = costado ? new int[]{2, 3} : new int[]{5, 6};
            return pos[ra];
        }
        return 0;
    }

    private int generarPos() {
        int ra = (int) (Math.random() * 3);
        if (dir == 1) {
            int[] pos = {4, 5, 6};
            return pos[ra];
        } else if (dir == 3) {
            int[] pos = {6, 7, 8};
            return pos[ra];
        } else if (dir == 5) {
            int[] pos = {8, 1, 2};
            return pos[ra];
        } else if (dir == 7) {
            int[] pos = {2, 3, 4};
            return pos[ra];
        }
        return 0;
    }
}
