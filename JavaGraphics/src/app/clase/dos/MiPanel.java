/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.clase.dos;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author amurilloa
 */
public class MiPanel extends JPanel implements KeyListener {
    
    private Bola balon;
//    private Bola balon2;

    public MiPanel() {
        addKeyListener(this);
        setFocusable(true);
        setBackground(Color.WHITE);
        balon = new Bola(600, 260, Color.BLUE);
//        balon2 = new Bola(120, 380, Color.GREEN);
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }
    
    @Override
    public void paint(Graphics gg) {
        super.paint(gg);
        Graphics2D g = (Graphics2D) gg;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        
        balon.pintar(g);
        balon.mover(getWidth(), getHeight());
//        balon2.pintar(g);
//        g.drawOval(45, 45, 100, 100);
        BasicStroke bs = new BasicStroke(1);
        g.setStroke(bs);
//
//        g.drawOval(45, 45, 100, 100);
//        g.setColor(Color.BLUE);
//        g.drawOval(50, 50, 100, 100);
//
//        g.drawLine(10, 10, 80, 80);
//        g.drawRect(250, 250, 250, 120);
//
//        g.fillArc(400, 400, 100, 100, 0, 180);
//        
//        g.setFont(new Font("ARIAL", Font.PLAIN, 30));
//        g.drawString("HOLA MUNDO", 200, 200);

        g.setColor(Color.RED);
        g.drawLine(400, 0, 400, 600);
        g.drawLine(0, 300, 800, 300);
        
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
        
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() >= KeyEvent.VK_0 && e.getKeyCode() <= KeyEvent.VK_9) {
            balon.cambiarColor(e.getKeyCode() - 48);
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            balon.setDir(7);
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            balon.setDir(3);
        } else if (e.getKeyCode() == KeyEvent.VK_UP) {
            balon.setDir(1);
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            balon.setDir(5);
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            balon.detener();
        }
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
        
    }
    
}

// Con los números del 1-5 van a cambiar el color de la bola
// Con los números del 6-0 van a cambiar el color del borde de la bola

// La bola tiene que rebotar en un orden natural.
