/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.clase.dos;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;

/**
 *
 * @author amurilloa
 */
public class Alcantarilla {

    private int x;
    private int y;
    private char tipo;
    private boolean entrar;
    private boolean enemigo;

    private final Color COLOR_UNO = new Color(31, 204, 18);
    private final Color COLOR_DOS = new Color(19, 128, 11);

    public Alcantarilla() {
    }

    public Alcantarilla(int x, int y, char tipo, boolean entrar, boolean enemigo) {
        this.x = x;
        this.y = y;
        this.tipo = tipo;
        this.entrar = entrar;
        this.enemigo = enemigo;
    }

    public void paint(Graphics2D g) {
        int tam = tipo == 'P' ? 70 : 140;

        //Proyección marco invisible con el que el objeto choca con otros elementos 
//        g.setColor(Color.BLACK);
//        g.drawRect(x, y, 145, tam + 30);

        g.setColor(COLOR_UNO);
        g.fillRoundRect(x, y, 145, 30, 15, 15);
        g.setColor(COLOR_UNO);
        g.fillRect(x + 5, y + 30, 135, tam);
        g.setColor(COLOR_DOS);
        g.fillRect(x + 40, y + 34, 3, tam - 10);
        g.fillRect(x + 64, y + 4, 5, tam + 20);
        g.fillRect(x + 72, y, 30, tam + 30);
        g.fillRect(x + 117, y, 5, tam + 30);

        g.setColor(Color.BLACK);
        g.setStroke(new BasicStroke(2));
        g.drawRect(x + 5, y + 30, 135, tam);
        g.drawRoundRect(x, y, 145, 30, 15, 15);
        g.setStroke(new BasicStroke(1));

    }

    public Rectangle getBounds() {
        int tam = tipo == 'P' ? 70 : 140;
        return new Rectangle(x, y, 145, tam + 30);
    }

}
