/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.clase.dos;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author amurilloa
 */
public class Mapa {
    
    private int x;
    private int y;
    
    private final Color CCAFE = new Color(163,90,59);
    private final Color CCIELO = new Color(89,145,255);
    
    public void paint(Graphics2D g) {
        g.setColor(CCAFE);
        g.fillRect(0, 500, 800, 100);
        g.setColor(CCIELO);
        g.fillRect(0, 0, 800, 500);
        
    }
}
