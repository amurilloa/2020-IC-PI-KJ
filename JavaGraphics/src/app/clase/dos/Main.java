/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.clase.dos;

import javax.swing.JFrame;

/**
 *
 * @author amurilloa
 */
public class Main {

    public static void main(String[] args) {
        JFrame frm = new JFrame("Clase Virtual #2");
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.add(new MiPanel());
        frm.setSize(800, 600);
        frm.setLocationRelativeTo(null);
        frm.setResizable(false);
        frm.pack();
        frm.setVisible(true);
        try {
            while (true) {
                frm.repaint();
                Thread.sleep(10);
            }
        } catch (InterruptedException ex) {
            System.out.println("Problemas al repintar la ventana");
        }

    }
}
