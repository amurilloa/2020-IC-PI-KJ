/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.clase.dos;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author amurilloa
 */
public class Bola {

    private int x;
    private int y;
    private int dir;
    private Color colorF;
    private Color colorB;
    private final int TAM = 80;
    private final int SPD = 4;

    public Bola(int x, int y, Color colorF) {
        this.x = x;
        this.y = y;
        this.colorF = colorF;
        colorB = Color.GRAY;
        dir = 0;
    }

    public void pintar(Graphics2D g) {
        g.setColor(colorF);
        g.fillOval(x, y, TAM, TAM);
        g.setColor(colorB);
        BasicStroke bs = new BasicStroke(4);
        g.setStroke(bs);
        g.drawOval(x, y, TAM, TAM);
    }

    public void mover(int ancho, int alto) {
        if (dir == 1) { //Arriba
            y -= SPD;
            if (y <= 0) {
                dir = generarPos();
            }
        } else if (dir == 2) { //Arr+Der
            x += SPD;
            y -= SPD;
            if (x + TAM >= ancho) {
                dir = generarPos(true);
            } else if (y <= 0) {
                dir = generarPos(false);
            }
        } else if (dir == 3) { //Derecha
            x += SPD;
            if (x + TAM >= ancho) {
                dir = generarPos();
            }
        } else if (dir == 4) { //Abj+Der
            x += SPD;
            y += SPD;
            if (x + TAM >= ancho) {
                dir = generarPos(true);
            } else if (y + TAM >= alto) {
                dir = generarPos(false);
            }

        } else if (dir == 5) { //Abajo
            y += SPD;
            if (y + TAM >= alto) {
                dir = generarPos();
            }
        } else if (dir == 6) { // Aba+Izq
            x -= SPD;
            y += SPD;
            if (x <= 0) {
                dir = generarPos(true);
            } else if (y + TAM >= alto) {
                dir = generarPos(false);
            }
        } else if (dir == 7) { //Izquierda
            x -= SPD;
            if (x <= 0) {
                dir = generarPos();
            }
        } else if (dir == 8) { //Arr+Iz
            x -= SPD;
            y -= SPD;
            if (x <= 0) {
                dir = generarPos(true);
            } else if (y <= 0) {
                dir = generarPos(false);
            }
        }
    }

    private int generarPos(boolean costado) {
        int ra = (int) (Math.random() * 2);
        if (dir == 2) {
            int[] pos = costado ? new int[]{7, 8} : new int[]{5, 4};
            return pos[ra];
        } else if (dir == 4) {
            int[] pos = costado ? new int[]{6, 7} : new int[]{1, 2};
            return pos[ra];
        } else if (dir == 6) {
            int[] pos = costado ? new int[]{3, 4} : new int[]{8, 1};
            return pos[ra];
        } else if (dir == 8) {
            int[] pos = costado ? new int[]{2, 3} : new int[]{5, 6};
            return pos[ra];
        }
        return 0;
    }

    private int generarPos() {
        int ra = (int) (Math.random() * 3);
        if (dir == 1) {
            int[] pos = {4, 5, 6};
            return pos[ra];
        } else if (dir == 3) {
            int[] pos = {6, 7, 8};
            return pos[ra];
        } else if (dir == 5) {
            int[] pos = {8, 1, 2};
            return pos[ra];
        } else if (dir == 7) {
            int[] pos = {2, 3, 4};
            return pos[ra];
        }
        return 0;
    }

    public void cambiarColor(int num) {
        switch (num) {
            case 1:
                colorF = Color.BLUE;
                break;
            case 2:
                colorF = Color.RED;
                break;
            case 3:
                colorF = Color.DARK_GRAY;
                break;
            case 4:
                colorF = Color.ORANGE;
                break;
            case 5:
                colorF = Color.GREEN;
                break;
            case 6:
                colorB = Color.GRAY;
                break;
            case 7:
                colorB = Color.YELLOW;
                break;
            case 8:
                colorB = Color.PINK;
                break;
            case 9:
                colorB = Color.MAGENTA;
                break;
            case 0:
                colorB = Color.CYAN;
                break;
        }
    }

//    public void mover(int ancho, int alto) {
//       if (dir == 1) {
//            y -= 5;
//            if (y - 5 <= 0) {
//                dir = 5;
//            }
//        } else if (dir == 5) {
//            y += 5;
//            if (y + 5 + TAM >= alto) {
//                dir = 1;
//            }
//        } else if (dir == 3) {
//            x += 5;
//            if (x + 5 + TAM >= ancho) {
//                dir = 7;
//            }
//        } else if (dir == 7) {
//            x -= 5;
//            if (x - 5 <= 0) {
//                dir = 3;
//            }
//        } else if (dir == 2) {
//            y -= 5;
//            x += 5;
//            if (x + 5 + TAM >= ancho) {
//                dir = 6;
//            } else if (y - 5 <= 0) {
//                dir = 6;
//            }
//        } else if (dir == 4) {
//            y += 5;
//            x += 5;
//            if (x + 5 + TAM >= ancho) {
//                dir = 8;
//            } else if (y + 5 + TAM >= alto) {
//                dir = 8;
//            }
//        } else if (dir == 6) {
//            y += 5;
//            x -= 5;
//            if (y + 5 + TAM >= alto) {
//                dir = 2;
//            } else if (x - 5 <= 0) {
//                dir = 2;
//            }
//        } else if (dir == 8) {
//            y -= 5;
//            x -= 5;
//            if (x - 5 <= 0) {
//                dir = 4;
//            } else if (y - 5 <= 0) {
//                dir = 4;
//            }
//        }
//        
//        if (x == 360 && y == 260 && dir != 0) {
//            //int[] ops = {1, 3, 4, 5, 7};
//            int ram = (int) (Math.random() * 8) + 1;
//            dir = ram;
//        }
//
//    }
    public void setDir(int dir) {
        this.dir = dir;
    }

    public void detener() {
        dir = 0;
        x = 360;
        y = 260;
    }

}
