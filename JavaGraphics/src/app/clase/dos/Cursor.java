/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.clase.dos;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author amurilloa
 */
public class Cursor {

    private int x;
    private int y;
    private int pos;
    private int lim;

    private final Color COLOR = Color.WHITE;

    public Cursor() {
    }

    public Cursor(int x, int y, int lim) {
        this.x = x;
        this.y = y;
        this.lim = lim;
    }

    public void paint(Graphics2D g) {

        int[] xs = {x, x, x + 40};
        int[] ys = {y + pos * 40, y + 40 + pos * 40, y + 20 + pos * 40};
        g.setColor(COLOR);
        g.fillPolygon(xs, ys, xs.length);

    }

    public int getPos() {
        return pos;
    }

    public void setLim(int lim) {
        this.lim = lim;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
    
    
    public void cambiarPos(int valor) {
        pos += valor;
        if (pos < 0) {
            pos = 0;
        } else if (pos >= lim) {
            pos = lim;
        }

    }

}
