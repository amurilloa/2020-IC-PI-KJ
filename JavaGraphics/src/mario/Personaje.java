/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mario;

import app.clase.dos.Alcantarilla;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class Personaje {

    private int x;
    private int y;
    private Color color;
    private int vis;
    private int dir;
    private int cuerpo;
    private int cant;

    private final int SPD = 5;
    private final Color CPIEL = new Color(255, 190, 90);
    private final Color CCAFE = new Color(106, 73, 40);

    public Personaje() {
        color = Color.RED;
        vis = 1;
    }

    public Personaje(int x, int y, Color color, int vis, int cuerpo) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.vis = vis;
        this.cuerpo = cuerpo;

    }

    public void paint(Graphics2D g) {
        //Proyección marco invisible con el que el objeto choca con otros elementos 
//        g.setColor(Color.BLACK);
//        g.drawRect(x + 10, y, 130, 160);

//        AffineTransform old = g.getTransform();
//        AffineTransform nw = AffineTransform.getScaleInstance(0.5, 0.5);
//        
//        g.transform(nw);
        g.setColor(color);
        g.fillRect(x + 50, y + 0, 50, 10);
        g.fillRect(x + 40, y + 10, 90, 10);

        g.setColor(CPIEL);
        g.fillRect(x + 40, y + 20, 70, 30);
        g.fillRect(x + 50, y + 40, 70, 30);
        g.fillRect(x + 110, y + 30, 20, 10);
        g.fillRect(x + 120, y + 40, 20, 10);

        g.setColor(CCAFE);
        g.fillRect(x + 40, y + 20, 30, 10);//Pelo
        g.fillRect(x + 30, y + 30, 10, 30);//Pelo atras
        g.fillRect(x + 40, y + 50, 10, 10);
        g.fillRect(x + 50, y + 30, 10, 20);//Patilla
        g.fillRect(x + 60, y + 40, 10, 10);
        g.fillRect(x + 90, y + 20, 10, 20);//Ojos
        g.fillRect(x + 100, y + 40, 10, 10);//Bigote
        g.fillRect(x + 90, y + 50, 40, 10);//Bigote

        if (cuerpo == 1) {
            cuerpoUno(g);
        } else if (cuerpo == 2) {
            cuerpoDos(g);
        } else if (cuerpo == 3) {
            cuerpoTres(g);
        }
//        g.transform(old);

    }

    public Rectangle getBounds() {
        return new Rectangle(x + 10, y, 130, 160);
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public void mover(LinkedList<Alcantarilla> alc) {
        int rx = x;
        int ry = y;
        cant++;
        if (cant >= 8) {
            cuerpo = cuerpo == 1 ? 1 : cuerpo == 2 ? 3 : 2;
            cant = 0;
        }
        if (dir == 1) {
            x -= SPD;
        } else if (dir == 2) {
            y -= SPD;
            if (y <= 100) {
                dir = 4;
            }
        } else if (dir == 3) {
            x += SPD;
        } else if (dir == 4) {
            y += SPD;
            if (y >= 340) {
                y = 340;
                dir = 0;
            }
        }
        for (Alcantarilla alcantarilla : alc) {
            if (getBounds().intersects(alcantarilla.getBounds())) {
                x = rx;
                y = ry;
                break;
            }
        }

    }

    public int getDir() {
        return dir;
    }

    public void setCuerpo(int cuerpo) {
        this.cuerpo = cuerpo;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public String getNombre() {
        return color == Color.RED ? "MARIO" : "LUIGI";
    }

    private void cuerpoUno(Graphics2D g) {
        g.setColor(CCAFE);
        g.fillRect(x + 40, y + 70, 60, 10);
        g.fillRect(x + 30, y + 80, 100, 10);
        g.fillRect(x + 10, y + 90, 130, 10);
        g.fillRect(x + 110, y + 100, 10, 10);
        g.fillRect(x + 40, y + 100, 10, 10);
        g.fillRect(x + 30, y + 140, 30, 10);
        g.fillRect(x + 10, y + 150, 50, 10);
        g.fillRect(x + 100, y + 140, 30, 10);
        g.fillRect(x + 100, y + 150, 40, 10);

        g.setColor(color);
        g.fillRect(x + 60, y + 70, 10, 10);
        g.fillRect(x + 60, y + 80, 10, 10);
        g.fillRect(x + 60, y + 90, 10, 10);

        g.fillRect(x + 70, y + 90, 10, 10);
        g.fillRect(x + 80, y + 90, 10, 10);
        g.fillRect(x + 80, y + 90, 10, 10);
        g.fillRect(x + 90, y + 80, 10, 10);
        g.fillRect(x + 90, y + 90, 10, 10);
        g.fillRect(x + 100, y + 100, 10, 10);
        g.fillRect(x + 80, y + 100, 10, 10);
        g.fillRect(x + 70, y + 100, 10, 10);
        g.fillRect(x + 50, y + 100, 10, 10);
        g.fillRect(x + 50, y + 110, 70, 10);
        g.fillRect(x + 40, y + 120, 90, 10);
        g.fillRect(x + 40, y + 130, 30, 10);
        g.fillRect(x + 90, y + 130, 30, 10);

        g.setColor(CPIEL);
        g.fillRect(x + 60, y + 100, 10, 10);
        g.fillRect(x + 90, y + 100, 10, 10);
        g.fillRect(x + 10, y + 100, 30, 10);//Mano Izq
        g.fillRect(x + 10, y + 110, 40, 10);
        g.fillRect(x + 10, y + 120, 30, 10);

        g.fillRect(x + 120, y + 100, 20, 10);//Mano Der
        g.fillRect(x + 110, y + 110, 30, 10);
        g.fillRect(x + 120, y + 120, 20, 10);

    }

    private void cuerpoDos(Graphics2D g) {
        g.setColor(CCAFE);
        g.fillRect(x + 20, y + 70, 80, 10);
        g.fillRect(x + 20, y + 80, 100, 10);
        g.fillRect(x + 40, y + 90, 90, 10);
        g.fillRect(x + 130, y + 100, 10, 10);//Punta del zapato
        g.fillRect(x + 120, y + 110, 20, 30);//Punta del zapato
        g.fillRect(x + 10, y + 130, 20, 20);

        g.setColor(color);
        g.fillRect(x + 60, y + 70, 20, 20);
        g.fillRect(x + 60, y + 90, 10, 10);
        g.fillRect(x + 80, y + 80, 10, 20);
        g.fillRect(x + 80, y + 90, 30, 40);
        g.fillRect(x + 90, y + 110, 30, 30);
        g.fillRect(x + 40, y + 100, 40, 30);
        g.fillRect(x + 30, y + 110, 30, 30);
        g.fillRect(x + 20, y + 120, 20, 20);

        g.setColor(CCAFE);
        g.fillRect(x + 10, y + 130, 20, 20);
        g.fillRect(x + 20, y + 140, 20, 20);
        g.fillRect(x + 40, y + 150, 10, 10);

        g.setColor(CPIEL);
        g.fillRect(x + 70, y + 90, 10, 10);
        g.fillRect(x, y + 80, 20, 30);//Mano Izq
        g.fillRect(x + 20, y + 90, 10, 10);

        g.fillRect(x + 120, y + 80, 10, 10);
        g.fillRect(x + 130, y + 80, 20, 20);//Mano Der

    }

    private void cuerpoTres(Graphics2D g) {
        g.setColor(CCAFE);
        g.fillRect(x + 40, y + 70, 60, 10);
        g.fillRect(x + 30, y + 80, 80, 10);
        g.fillRect(x + 30, y + 90, 30, 10);
        g.fillRect(x + 30, y + 100, 40, 10);
        g.fillRect(x + 40, y + 110, 20, 10);
        g.fillRect(x + 50, y + 120, 10, 10);

        g.fillRect(x + 80, y + 130, 30, 10);
        g.fillRect(x + 50, y + 140, 70, 10);
        g.fillRect(x + 50, y + 150, 40, 10);

        g.setColor(color);
        g.fillRect(x + 30, y + 110, 10, 10);
        g.fillRect(x + 40, y + 120, 10, 10);
        g.fillRect(x + 50, y + 130, 30, 10);
        g.fillRect(x + 80, y + 120, 30, 10);
        g.fillRect(x + 60, y + 70, 10, 10);
        g.fillRect(x + 70, y + 80, 20, 10);
        g.fillRect(x + 60, y + 90, 50, 10);
        g.fillRect(x + 70, y + 100, 50, 10);
        g.fillRect(x + 90, y + 110, 30, 10);

        g.setColor(CPIEL);
        g.fillRect(x + 80, y + 90, 10, 10);
        g.fillRect(x + 110, y + 90, 10, 10);
        g.fillRect(x + 60, y + 110, 30, 10);
        g.fillRect(x + 60, y + 120, 20, 10);
    }

}
