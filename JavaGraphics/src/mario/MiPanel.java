/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mario;

import app.clase.dos.*;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import javax.swing.JPanel;

/**
 *
 * @author amurilloa
 */
public class MiPanel extends JPanel implements KeyListener {

    private final Personaje mario;
    private final Mapa mapa;
    private LinkedList<Alcantarilla> alcantarillas;
    private final Cursor flecha;
    private boolean inicio;
    private boolean salir;
    private int tiempo;
    private int cant;
    private int dirResp;

    public MiPanel() {
        addKeyListener(this);
        setFocusable(true);
        setBackground(Color.WHITE);
        mario = new Personaje(0, 340, Color.RED, 1, 1);
        mapa = new Mapa();
        alcantarillas = new LinkedList<>();
        alcantarillas.add(new Alcantarilla(650, 400, 'P', true, true));
        alcantarillas.add(new Alcantarilla(350, 330, 'G', true, true));
        flecha = new Cursor(240, 220, 2);

    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }

    @Override
    public void paint(Graphics gg) {
        super.paint(gg);
        Graphics2D g = (Graphics2D) gg;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        mapa.paint(g);
        mario.paint(g);
        if (tiempo <= 0 && inicio) {
            g.setColor(Color.WHITE);
            g.setFont(new Font("DUMMY", Font.BOLD, 24));
            g.drawString("  GAME OVER", 300, 250);
        } else {
            if (inicio && !salir) {

                mario.mover(alcantarillas);

                cant += 10;
                if (cant >= 1000) {
                    tiempo--;
                    cant = 0;
                }
            } else if (inicio && salir) {
                g.setColor(Color.WHITE);
                g.setFont(new Font("DUMMY", Font.BOLD, 24));
                g.drawString("1. RESUME", 300, 250);
                g.drawString("2. EXIT", 300, 290);
                flecha.paint(g);
            }
        }
        g.setColor(Color.WHITE);
        g.setFont(new Font("DUMMY", Font.BOLD, 24));

        g.drawString(mario.getNombre(), 50, 25);
        g.drawString("000000", 50, 50);
        g.drawString("TIME", 680, 25);
        g.drawString(String.format("%03d", tiempo), 690, 50);

        if (!inicio) {
            g.drawString("1. PLAYER GAME", 300, 250);
            g.drawString("2. PLAYER GAME", 300, 290);
            g.drawString("3. EXIT", 300, 330);
            flecha.paint(g);
        }
        for (Alcantarilla alcantarilla : alcantarillas) {
            alcantarilla.paint(g);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (tiempo <= 0 && inicio) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                salir = false;
                tiempo = 0;
                inicio = false;
            }
        } else {
            if (inicio) {
                if (e.getKeyCode() == KeyEvent.VK_E) {
                    salir = true;
                    flecha.setLim(1);
                    flecha.setPos(0);
                }
                if (salir) {
                    if (e.getKeyCode() == KeyEvent.VK_UP) {
                        flecha.cambiarPos(-1);
                    } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                        flecha.cambiarPos(1);
                    } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        int op = flecha.getPos();
                        if (op == 0) {
                            salir = false;
                        } else {
                            System.exit(0);
                        }
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    dirResp = mario.getDir() == 2 || mario.getDir() == 4 ? mario.getDir() : 4;
                    mario.setDir(e.getKeyCode() - 36);
                    mario.setCuerpo(2);
                } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                    System.out.println("SALTAR");
                    mario.setDir(2);
                }
            } else {
                if (e.getKeyCode() == KeyEvent.VK_UP) {
                    flecha.cambiarPos(-1);
                    cambiarColor();
                } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    flecha.cambiarPos(1);
                    cambiarColor();
                } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    int op = flecha.getPos();
                    switch (op) {
                        case 0:
                        case 1:
                            tiempo = 10;
                            inicio = true;
                            break;
                        case 2:
                            System.exit(0);
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (inicio && !(e.getKeyCode() == KeyEvent.VK_SPACE)) {
            mario.setDir(dirResp);
            mario.setCuerpo(1);
        }

    }

    private void cambiarColor() {
        int op = flecha.getPos();
        if (op == 1) {
            mario.setColor(Color.GREEN);
        } else {
            mario.setColor(Color.RED);
        }
    }
}

// Con los números del 1-5 van a cambiar el color de la bola
// Con los números del 6-0 van a cambiar el color del borde de la bola
// La bola tiene que rebotar en un orden natural.
