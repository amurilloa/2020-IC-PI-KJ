/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javagraphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author amurilloa
 */
public class MiPanel extends JPanel {

    private Circulo bola; 

    public MiPanel() {
        bola = new Circulo(380, 305, Color.GREEN, 5);
        
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        bola.paint(g);
        bola.mover(getHeight(),getWidth());
        
        g.drawLine(390, 290, 0, 0); //Dibujar lineas rectas
        g.drawLine(410, 290, 800, 0);
        g.drawLine(410, 310, 800, 600);
        g.drawLine(390, 310, 0, 600);
        
        g.setColor(Color.BLUE);
        g.fillOval(360, 50, 80, 80); //Circulo Relleno
        g.drawOval(360, 260, 80, 80); //Borde de un circulo
//
        g.setColor(new Color(164, 125, 255));
        g.fillRect(20, 20, 200, 100); //Rectangulo Relleno
        g.drawRect(20, 20, 200, 100);// Borde de un Rectangulo 
//
        g.setColor(Color.CYAN);

        int[] xs = {400, 350, 450};
        int[] ys = {213, 300, 300};
        g.fillPolygon(xs, ys, xs.length);
//
//        g.setColor(Color.BLUE);
//        g.fillArc(50, 350, 100,100, 720, 45);
//        
//        g.setFont(new Font("DUMMY", Font.BOLD, 28));
//        g.drawString("Hola Mundo", 200, 200);
//       
//        Carro c = new Carro(180, 300, Color.BLUE, Color.WHITE, true);
//        c.paint(g);
//        Carro c1 = new Carro(100, 400, Color.MAGENTA, Color.BLUE, true);
//        c1.paint(g);
     
      
        
        //Ejes X , Y
        g.setColor(Color.RED); //Cambiar el color
        g.drawLine(400, 0, 400, 600);
        g.drawLine(0, 300, 800, 300);

    }
}
