/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javagraphics;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author amurilloa
 */
public class Circulo {

    private int x;
    private int y;
    private Color color;
    private final int TAM = 40;
    private final int VEL = 5;
    private int dir;//0, 1,2, 3, 4, 5 ,6 7, 8

    public Circulo() {
    }

    public Circulo(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public Circulo(int x, int y, Color color, int dir) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.dir = dir;
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, TAM, TAM);
    }

    public void mover(int alto, int ancho) {

        //La bola rebote siempre que llegue a un borde, buscando el centro 
        if ((dir == 1 || dir == 5) && y == alto / 2) {
            int random = (int) (Math.random() * 4);
            if (random % 2 == 0) {
                random += 1;
            } else {
                random += 4;
            }
            dir = random;
        }

        if (dir == 1) { //Arriba
            if (y > 0) {
                y -= VEL;
            } else {
                dir = 5;
            }
        } else if (dir == 3) { //Derecha
            x += VEL;
        } else if (dir == 5) {//Abajo
            if (y + TAM < alto) {
                y += VEL;
            } else {
                dir = 1;
            }
        } else if (dir == 7) {//Izquierda 
            x -= VEL;
        }

    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

}
