/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javagraphics;

import javax.swing.JFrame;

/**
 *
 * @author amurilloa
 */
public class JavaGraphics {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        JFrame ventana = new JFrame("JavaGraphics - v0.1");
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.add(new MiPanel()); //Agregar un panel propio
        ventana.setSize(800, 600); //Tamaño 
        ventana.setLocationRelativeTo(null); //Centrar la ventana
        ventana.pack(); //Valida o confirma que todo este correcto en la interfaz
        ventana.setVisible(true); //Muestra la ventana 
        ventana.setResizable(false);
        while(true){
            ventana.repaint();
            Thread.sleep(15);
        }
        
    }
    
}
