/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javagraphics;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author amurilloa
 */
public class Carro {

    private int x;
    private int y;
    private Color colorP;
    private Color colorS;
    private boolean encendido;

    public Carro() {
    }

    public Carro(int x, int y, Color colorP, Color colorS, boolean encendido) {
        this.x = x;
        this.y = y;
        this.colorP = colorP;
        this.colorS = colorS;
        this.encendido = encendido;
    }

    public void paint(Graphics g) {
        int[] xs = {x + 50, x + 80, x + 95, x + 165, x + 195, x + 245, x + 245, x + 210, x + 50};
        int[] ys = {y + 25, y + 25, y + 0, y + 0, y + 25, y + 35, y + 60, y + 65, y + 65};

        //Mufla
        g.setColor(Color.DARK_GRAY);
        g.fillRect(x+45, y+60, 15, 5);
        
        //Carro
        g.setColor(colorP);
        g.fillPolygon(xs, ys, xs.length);

        //LLantas
        g.setColor(Color.BLACK);
        g.fillOval(x + 190, y + 45, 40, 40);
        g.fillOval(x + 80, y + 45, 40, 40);

        //Detalle de las ventanas
        g.setColor(colorS);
        g.drawLine(x + 84, y + 25, x + 191, y + 25);
        g.drawLine(x + 125, y + 4, x + 125, y + 21);

        //Luz de Freno
        g.setColor(Color.RED);
        g.fillRect(x + 52, y + 27, 5, 10);

        //Luz Frontal
        g.setColor(Color.YELLOW);
        g.fillRect(x + 235, y + 35, 8, 8);

        if(encendido){
            g.setColor(Color.GRAY);
            g.fillOval(x+37, y+60, 8, 5);
            g.fillOval(x+31, y+57, 8, 5);
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColorP() {
        return colorP;
    }

    public void setColorP(Color colorP) {
        this.colorP = colorP;
    }

    public Color getColorS() {
        return colorS;
    }

    public void setColorS(Color colorS) {
        this.colorS = colorS;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }

}
