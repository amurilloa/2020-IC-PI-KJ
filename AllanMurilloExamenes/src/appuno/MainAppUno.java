/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appuno;

/**
 *
 * @author amurilloa
 */
public class MainAppUno {

    public static void main(String[] args) {
        
        Logica log = new Logica();

        String menu = "1. Registrar Examen\n"
                + "2. Registrar Pregunta\n"
                + "3. Elminar Examen\n"
                + "4. Calcular Puntaje\n"
                + "5. Salir";

        APP:
        while (true) {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    int cod = log.generar() + 1;
                    String nom = Util.leer("Nombre del Examen(" + cod + "): ");
                    int nota = Util.leerInt("Nota Mínima: ");
                    Examen e = new Examen(cod, nom, nota);
                    log.regExamen(e);
                    break;
                case 2:
                    int numExa = Util.menuOpciones(log.exaDisp(), "Seleccione un examen");
                    Examen ex = log.getExamen(numExa);
                    int num = ex.generar() + 1;
                    String preg = Util.leer("Pregunta # " + num + ": ");
                    String resp = Util.leer("Pregunta: " + preg + "\nRespuesta: ");
                    String opcUno = Util.leer("Pregunta: " + preg + "\nRespuesta: " + resp + "\nOpción 1: ");
                    String opcDos = Util.leer("Pregunta: " + preg + "\nRespuesta: " + resp + "\nOpción 1: " + opcUno + "\nOpción 2: ");
                    String opcTres = Util.leer("Pregunta: " + preg + "\nRespuesta: " + resp + "\nOpción 1: " + opcUno + "\nOpción 2: " + opcDos + "\nOpción 3: ");
                    int pun = Util.leerInt("Pregunta: " + preg + "\nRespuesta: " + resp + "\nOpción 1: " + opcUno + "\nOpción 2: " + opcDos + "\nOpción 3: " + opcTres + "\nPuntaje: ");
                    Pregunta p = new Pregunta(num, preg, resp, opcUno, opcDos, opcTres, pun);
                    ex.regPregunta(p);
                    log.guardarPre();
                    break;
                case 3:
                    numExa = Util.menuOpciones(log.exaDisp(), "Seleccione un examen");
                    log.delExamen(numExa);
                    break;
                case 4:
                    numExa = Util.menuOpciones(log.exaDisp(), "Seleccione un examen");
                    ex = log.getExamen(numExa);
                    pun = Util.leerInt("Nota Mínima(" + ex.getNotaMin() + "): ");
                    ex.setNotaMin(pun);
                    for (Pregunta pregunta : ex.getPreguntas()) {
                        if (pregunta != null) {
                            pregunta.setPuntaje(Util.leerInt(String.valueOf(pregunta)));
                        }
                    }
                    log.guardarExa();
                    log.guardarPre();
                    break;
                case 5:
                    break APP;
            }
        }

    }
}
