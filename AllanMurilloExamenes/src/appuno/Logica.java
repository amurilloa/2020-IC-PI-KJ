/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appuno;

import appdos.Usuario;

/**
 *
 * @author amurilloa
 */
public class Logica {

    private final Usuario[] usuarios;
    private final Examen[] examenes;
    private final ManejoArchivos ma;
    private final String RUTA_EXA = "examenes/examenes.txt";
    private final String RUTA_PRE = "examenes/preguntas.txt";
    private final String RUTA_USU = "examenes/usuarios.txt";
    private final String RUTA_APR = "examenes/aprobados.txt";

    public Logica() {
        ma = new ManejoArchivos();
        examenes = new Examen[100];
        usuarios = new Usuario[10];
        carExa();
        carPre();
        carUsu();
    }

    private void carExa() {
        String datos = ma.leer(RUTA_EXA);
        String[] lineas = datos.split("\n");
        for (String linea : lineas) {
            String[] aux = linea.split(",");
            int cod = Integer.parseInt(aux[0]);
            String nom = aux[1];
            int nota = Integer.parseInt(aux[2]);
            Examen e = new Examen(cod, nom, nota);
            regExamen(e);
        }
    }

    private void carPre() {
        String datos = ma.leer(RUTA_PRE);
        String[] lineas = datos.split("\n");
        for (String linea : lineas) {
            String[] aux = linea.split(",");

            int cod = Integer.parseInt(aux[0]);
            Examen ex = getExamenCod(cod);
            int num = Integer.parseInt(aux[1]);
            String preg = aux[2];
            String resp = aux[3];
            String opcUno = aux[4];
            String opcDos = aux[5];
            String opcTres = aux[6];
            int pun = Integer.parseInt(aux[7]);
            Pregunta p = new Pregunta(num, preg, resp, opcUno, opcDos, opcTres, pun);
            ex.regPregunta(p);
            guardarPre();
        }
    }

    public void regExamen(Examen e) {
        for (int i = 0; i < examenes.length; i++) {
            if (examenes[i] == null) {
                examenes[i] = e;
                break;
            }
        }
        guardarExa();
    }

    public void guardarExa() {
        String txt = "";
        for (Examen exa : examenes) {
            if (exa != null) {
                txt += exa.getData();
            }
        }
        ma.escribir(RUTA_EXA, txt);
    }

    public void guardarPre() {
        String txt = "";
        for (Examen exa : examenes) {
            if (exa != null) {
                for (Pregunta pre : exa.getPreguntas()) {
                    if (pre != null) {
                        txt += exa.getCod() + "," + pre.getData();
                    }
                }
            }
        }
        ma.escribir(RUTA_PRE, txt);
    }

    public int generar() {
        for (int i = 0; i < examenes.length; i++) {
            if (examenes[i] == null) {
                return i;
            }
        }
        return -1;
    }

    public String[] exaDisp() {
        String txt = "";
        for (Examen examen : examenes) {
            if (examen != null) {
                txt += examen.getCod() + "." + examen.getNombre() + ",";
            }
        }
        return txt.split(",");
    }

    public Examen getExamen(int pos) {
        for (Examen exa : examenes) {
            if (exa != null) {
                if (pos == 0) {
                    return exa;
                }
                pos--;
            }
        }
        return null;
    }

    public Examen getExamenCod(int cod) {
        for (Examen exa : examenes) {
            if (exa != null && exa.getCod() == cod) {
                return exa;
            }
        }
        return null;
    }

    public void delExamen(int pos) {
        for (int i = 0; i < examenes.length; i++) {
            if (examenes[i] != null) {
                if (pos == 0) {
                    examenes[i] = null;
                }
                pos--;
            }
        }
        guardarExa();
        guardarPre();
    }
// APP-DOS

    public boolean existe(String usu) {
        for (Usuario usuario : usuarios) {
            if (usuario != null && (usu.equals(usuario.getUsuario()) || usu.equals(usuario.getCorreo()))) {
                return true;
            }
        }
        return false;
    }

    public void regUsuario(Usuario u) {
        for (int i = 0; i < usuarios.length; i++) {
            if (usuarios[i] == null) {
                usuarios[i] = u;
                break;
            }
        }
        guardarUsu();
    }

    public void guardarUsu() {
        String txt = "";
        for (Usuario usu : usuarios) {
            if (usu != null) {
                txt += usu.getData();
            }
        }
        ma.escribir(RUTA_USU, txt);
    }

    public Usuario login(String usu, String pas) {
        for (Usuario usuario : usuarios) {
            if (usuario != null && pas.equals(usuario.getPass())
                    && (usu.equals(usuario.getUsuario()) || usu.equals(usuario.getCorreo()))) {
                return usuario;
            }
        }
        return null;
    }

    private void carUsu() {
        String datos = ma.leer(RUTA_USU);
        String[] lineas = datos.split("\n");
        for (String linea : lineas) {
            String[] aux = linea.split(",");
            String us = aux[0];
            String cor = aux[1];
            String pas = aux[2];
            String nom = aux[3];
            Usuario u = new Usuario(us, cor, pas, nom);
            regUsuario(u);
        }
    }

    public String[] exaSinApr(String usu) {
        String txt = "";
        for (Examen examen : examenes) {
            if (examen != null && !existaApr(examen.getCod(), usu)) {
                txt += examen.getCod() + " " + examen.getNombre() + ",";
            }
        }
        return txt.split(",");
    }

    private boolean existaApr(int cod, String usu) {
        String aprobados = ma.leer(RUTA_APR);
        String[] lineas = aprobados.split("\n");
        for (String linea : lineas) {
            if (linea.startsWith(usu + "," + cod + ",")) {
                return true;
            }
        }
        return false;
    }

    public int calcularNota(Examen ex, int pun, String usuario) {
        int total = 0;
        for (Pregunta pregunta : ex.getPreguntas()) {
            if (pregunta != null) {
                total += pregunta.getPuntaje();
            }
        }
        if (total == 0) {
            return 0;
        }
        int nota = pun * 100 / total;
        if (nota >= ex.getNotaMin()) {
            guardarApr(usuario, ex.getCod(), nota);
        }
        return nota;
    }

    private void guardarApr(String usuario, int cod, int nota) {
        String datos = ma.leer(RUTA_APR).trim() + "\n";
        datos += usuario + "," + cod + "," + nota + "\n";
        ma.escribir(RUTA_APR, datos);
    }

}
