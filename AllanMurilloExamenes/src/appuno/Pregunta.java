/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appuno;

/**
 *
 * @author amurilloa
 */
public class Pregunta {

    private int numero;
    private String pregunta;
    private String respuesta;
    private String opcionUno;
    private String opcionDos;
    private String opcionTres;
    private int puntaje;

    public Pregunta() {
        puntaje = 1;
    }

    public Pregunta(int numero, String pregunta, String respuesta, String opcionUno, String opcionDos, String opcionTres, int puntaje) {
        this.numero = numero;
        this.pregunta = pregunta;
        this.respuesta = respuesta;
        this.opcionUno = opcionUno;
        this.opcionDos = opcionDos;
        this.opcionTres = opcionTres;
        this.puntaje = puntaje;
    }

    public String getData() {
        return String.format("%d,%s,%s,%s,%s,%s,%d\n", numero, pregunta, respuesta, opcionUno, opcionDos, opcionTres, puntaje);
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getOpcionUno() {
        return opcionUno;
    }

    public void setOpcionUno(String opcionUno) {
        this.opcionUno = opcionUno;
    }

    public String getOpcionDos() {
        return opcionDos;
    }

    public void setOpcionDos(String opcionDos) {
        this.opcionDos = opcionDos;
    }

    public String getOpcionTres() {
        return opcionTres;
    }

    public void setOpcionTres(String opcionTres) {
        this.opcionTres = opcionTres;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    public int getPuntaje() {
        return puntaje;
    }

    @Override
    public String toString() {
        return "Pregunta # " + numero + " - " + pregunta
                + "\nRespuesta: " + respuesta
                + "\nOpción 1: " + opcionUno
                + "\nOpción 2: " + opcionDos
                + "\nOpción 3: " + opcionTres
                + "\nPuntaje: " + puntaje;
    }

    public String[] getOpciones() {
        String[] aux = {respuesta, opcionUno, opcionDos, opcionTres};
        int[] pos = genOpc();
        return new String[]{aux[pos[0]], aux[pos[1]], aux[pos[2]], aux[pos[3]]};
    }

    private int[] genOpc() {
        int[] opcs = {-1, -1, -1, -1};
        int pos = 4;
        RAN:
        while (pos > 0) {
            int ram = (int) (Math.random() * 4);
            for (int opc : opcs) {
                if (opc == ram) {
                    continue RAN;
                }
            }
            opcs[pos - 1] = ram;
            pos--;
        }
        return opcs;
    }
}
