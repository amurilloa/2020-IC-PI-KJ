/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appuno;

/**
 *
 * @author amurilloa
 */
public class Examen {

    private int cod;
    private String nombre;
    private int notaMin;
    private Pregunta[] preguntas;

    public Examen() {
        this.preguntas = new Pregunta[100];
    }

    public Examen(int cod, String nombre, int notaMin) {
        this.cod = cod;
        this.nombre = nombre;
        this.notaMin = notaMin;
        this.preguntas = new Pregunta[100];
    }

    public void regPregunta(Pregunta p) {
        for (int i = 0; i < preguntas.length; i++) {
            if (preguntas[i] == null) {
                preguntas[i] = p;
                break;
            }
        }
    }

    public int generar() {
        for (int i = 0; i < preguntas.length; i++) {
            if (preguntas[i] == null) {
                return i;
            }
        }
        return -1;
    }

    public String getData() {
        return String.format("%d,%s,%d\n", cod, nombre, notaMin);
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNotaMin() {
        return notaMin;
    }

    public void setNotaMin(int notaMin) {
        this.notaMin = notaMin;
    }

    public Pregunta[] getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(Pregunta[] preguntas) {
        this.preguntas = preguntas;
    }

    @Override
    public String toString() {
        return "Examen{" + "cod=" + cod + ", nombre=" + nombre + ", notaMin=" + notaMin + ", preguntas=" + preguntas + '}';
    }
}
