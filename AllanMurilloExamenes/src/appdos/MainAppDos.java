/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appdos;

import appuno.Examen;
import appuno.Logica;
import appuno.Pregunta;
import appuno.Util;

/**
 *
 * @author amurilloa
 */
public class MainAppDos {

    public static void main(String[] args) {
        Logica log = new Logica();
        String menu = "1. Resolver Examen\n"
                + "2. Cerrar Sesión\n"
                + "3. Salir";

        APP:
        while (true) {
            String usu = Util.leer("Usuario/Email");
            if (!log.existe(usu)) {
                String us = Util.leer("Usuario");
                String cor = Util.leer("Correo");
                String nom = Util.leer("Nombre completo");
                String pas = Util.leer("Contraseña");
                Usuario u = new Usuario(us, cor, pas, nom);
                log.regUsuario(u);
            }
            String pas = Util.leer("Contraseña:");
            Usuario u = log.login(usu, pas);
            if (u != null) {
                Util.mostrar("Bienvenido, " + u.getNombre());
                EXA:
                while (true) {
                    int op = Util.leerInt(menu);
                    switch (op) {
                        case 1:
                            String[] opciones = log.exaSinApr(u.getUsuario());
                            int numExa = Util.menuOpciones(opciones, "Seleccione un examen");
                            System.out.println(opciones[numExa].split(" ")[0]);
                            Examen ex = log.getExamenCod(Integer.parseInt(opciones[numExa].split(" ")[0]));
                            
                            while (true) {
                                int pun = 0;
                                for (Pregunta p : ex.getPreguntas()) {
                                    if (p != null) {
                                        String[] opc = p.getOpciones();
                                        int res = Util.menuOpciones(opc, p.getPregunta() + "(" + p.getPuntaje() + "pt/pts)");
                                        if (p.getRespuesta().equals(opc[res])) {
                                            pun += p.getPuntaje();
                                        }
                                    }
                                }
                                int nota = log.calcularNota(ex, pun,u.getUsuario());
                                Util.mostrar("Nota del examen: " + nota);
                                if (nota >= ex.getNotaMin() || !Util.confirmar("¿ Desea repetir el examen ?")) {
                                    break;
                                }
                            }
                            break;
                        case 2:
                            break EXA;
                        case 3:
                            break APP;
                    }
                }
            }
        }
    }
}
