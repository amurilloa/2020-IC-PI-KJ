/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appdos;

/**
 *
 * @author amurilloa
 */
public class Usuario {

    private String usuario;
    private String correo;
    private String pass;
    private String nombre;

    public Usuario() {
    }

    public Usuario(String usuario, String correo, String pass, String nombre) {
        this.usuario = usuario;
        this.correo = correo;
        this.pass = pass;
        this.nombre = nombre;
    }

    public String getData() {
        return String.format("%s,%s,%s,%s\n", usuario, correo, pass, nombre);
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Usuario{" + "usuario=" + usuario + ", correo=" + correo + ", pass=" + pass + ", nombre=" + nombre + '}';
    }

}
