/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matricesstrings;

/**
 *
 * @author amurilloa
 */
public class Logica {

    public String corregir(String nombre) {
        nombre = nombre.toLowerCase();
        String[] datos = nombre.split(" ");
        String nuevo = "";
        for (String dato : datos) {
            nuevo += Character.toUpperCase(dato.charAt(0)) + dato.substring(1) + " ";
        }
        return nuevo.trim();
    }

    public boolean verificar(String pass) {
        int min = 0;
        int may = 0;
        int num = 0;
        int car = 0;
        int lar = 0;

        if (pass.length() >= 8) {
            lar = 1;
        }

        //pass = "hola"
        for (int i = 0; i < pass.length(); i++) {
            if (Character.isUpperCase(pass.charAt(i))) {
                may = 1;
            }

            if (Character.isLowerCase(pass.charAt(i))) {
                min = 1;
            }

            if (Character.isDigit(pass.charAt(i))) {
                num = 1;
            }

            if (!Character.isDigit(pass.charAt(i))
                    && !Character.isLetter(pass.charAt(i))) {
                car = 1;
            }
        }

        return min+may+car+lar+num == 5;
    }

}
