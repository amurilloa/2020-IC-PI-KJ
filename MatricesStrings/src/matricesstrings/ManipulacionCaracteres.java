/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matricesstrings;

import java.util.Arrays;

/**
 *
 * @author amurilloa
 */
public class ManipulacionCaracteres {

    public static void main(String[] args) {
        // 0 1 2 3 4 5 6 7 8 9 
        // H O L A   M U N D O
        
        String nom = "AlLaN mUrIlLo AlFaRo";
        System.out.println(nom);
        System.out.println("\033[31m\u2620 amuri\033[34mllo88 \u2620");
        System.out.println("Allan Murillo Alfaro");
        
        //Mayuscula o minuscula
        System.out.println(nom.toLowerCase()); //Todo el string a minuscula
        System.out.println(nom.toUpperCase()); //Todo el string a mayuscula
        nom = nom.toLowerCase();
        nom = "HOLA MUNDO DE JUANCHO";
        System.out.println(nom.length()); //Retornar cantidad elementos en string
        System.out.println(nom.charAt(9)); //Retorna la letra o caracter en un indice
        
        String[] palabras = nom.split(" ");
        System.out.println(Arrays.toString(palabras));
        System.out.println(palabras[2]);
        
        System.out.println(nom.substring(5, 11));//[5,11[
        
        nom = "HOLA MARIA";
        
        int a = 10;
        
        int b = 11;
        System.out.println(a==b);
        System.out.println(b==a);
        
        System.out.println("Hola".equals(nom));
        System.out.println("Hola".equalsIgnoreCase(nom));
       
        System.out.println(nom.contains("la"));
        System.out.println(nom.replaceAll("[^aeiouAEIOU]", "x"));
        //Expresiones regulares: https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
        
        String correo = "allanmnual@gmail.com ";
        String pass = " asdq we123 \t";
        String repass = " asdq we123 \t";
        System.out.println(pass);
        System.out.println(pass.trim());
        
        char letra = '\u2620';
        
        
        System.out.println(Character.isDigit(letra));
        System.out.println(Character.isLowerCase(letra));
        
        Logica log = new Logica();
        nom = "AlLaN roBerto mUrIlLo AlFaRo"; //Allan Murillo Alfaro
        System.out.println(nom);
        System.out.println(log.corregir(nom));//Allan Murillo Alfaro
        
        pass = "asa%12A3"; 
        System.out.println(log.verificar(pass)); // true o false
        //Tiene minimo 1 min, 1 may, 1 num y caracter especial, con una longitud de 8 o más 
        
        
        
        
        
        
         
//        HOLA -> LA True
//        LA -> HOLA False
//        for (int i = 0; i < 20; i++) {
//            System.out.println("\033["+(i+30)+"mColor: " + (i+30) + " Hola Mundo");
//        }
    }
}
